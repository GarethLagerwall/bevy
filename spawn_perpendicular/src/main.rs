use bevy::{
    prelude::*,
    sprite::{MaterialMesh2dBundle, Mesh2dHandle},
};
use rand::prelude::*;
//use bevy_rand::prelude::GlobalEntropy;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_systems(Startup, (setup,
                               createrect.after(setup),
                               query_system.after(createrect)))
        .add_systems(Update,
                     (spawnperp,
                      project_positions.after(spawnperp)))
        .run();
}


/// spawn suare pixels perpendicular to rect
fn spawnperp(
    //mut rng: ResMut<GlobalEntropy<WyRand>>,
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    query_rect: Query<(Entity, &Name, &FromPos, &ToPos)>,
){
    info!("Creating perp pixel suares");
    for (entity, name , frompos, topos) in &query_rect{
        //info!("{} {} {}",name.0,frompos.0,topos.0);
        let length = ((frompos.0[0]-topos.0[0]).powi(2)+(frompos.0[1]-topos.0[1]).powi(2)).sqrt();
        let unitvec = Vec3::new(frompos.0[0]-topos.0[0],frompos.0[1]-topos.0[1],0.)/length;
        
        let normvec = Vec3::new(-unitvec[1],unitvec[0],0.);
        let mut rng = rand::thread_rng();
        let randomnum = rng.gen_range(-length/4. ..=length/4.);
        //info!("Length: {length}\nrandom number: {randomnum}");
        let spawnpos = frompos.0 + randomnum*normvec;
        //Vec3::new(frompos.0[0],frompos.0[1] + (-unitvec[0]*randomnum), 0.);
        
        // generate rectangle
        let squarepix = commands.spawn(MaterialMesh2dBundle{
            mesh: Mesh2dHandle(meshes.add(Rectangle::new(2., 2.))), 
            material: materials.add(WHITE),
            ..default()
        })
        .insert(Position(spawnpos))
        .id();
        // add to parent
        commands.entity(entity).push_children(&[squarepix]);
    }
}

fn createrect(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    query_names: Query<(&Name, &Position)>,
    mut query_rect: Query<(Entity, &mut FromPos, &mut ToPos, &FromSupply, &ToSink, &Width)>,
    //query_rect: Query<(&FromSupply, &ToSink)>,
){
    info!("CREATING RECTANGLE");
    for (entity, mut frompos, mut topos, fromsup, tosink, basewidth) in &mut query_rect{
    //for (fromsup, tosink) in &query_rect{
        info!("{},{}",fromsup.0,tosink.0);
        let mut startpos = Vec3::new(0.,0.,0.) ;
        let mut endpos = Vec3::new(0.,0.,0.);
        for (name, position) in &query_names{
            if name.0 == fromsup.0{
                info!("Matched From: {} and Name: {}",fromsup.0,name.0);
                startpos = position.0;
            }
            else if name.0 == tosink.0{
                info!("Matched To: {} and Name: {}",tosink.0,name.0);
                endpos = position.0;
            }
            //info!("\n\nENTITY NAME: {} Position {} \n\n",endpos, startpos);
        }
        //Changing the from and to positions to reflect the associated circles
        *frompos = FromPos(startpos);
        *topos = ToPos(endpos);
        info!("new startpos: {}, endpos: {}", frompos.0, topos.0);

        //
        // create transforms etc. for rectangle
        //
        //info!("{}{}",startpos[0],endpos[0]);
        let length = ((startpos[0]-endpos[0]).powi(2)+(startpos[1]-endpos[1]).powi(2)).sqrt();
        let centrepoint  = ((startpos[0] + endpos[0]) / 2.0, (startpos[1] + endpos[1]) / 2.0);
        let degree = ((startpos[1] - endpos[1]) / (startpos[0] - endpos[0])).atan();
        info!("length: {}, centre: {:?}, rotation: {}", length, centrepoint, degree);
        // transforms
        let mut recttransform = Transform::from_xyz(centrepoint.0, centrepoint.1, -1.0);
        recttransform.rotation = Quat::from_rotation_z(degree);

        // generate rectangle
        let rectgraphic = commands.spawn(MaterialMesh2dBundle{
            mesh: Mesh2dHandle(meshes.add(Rectangle::new(length, basewidth.0))), 
            material: materials.add(NAVY),
            transform: recttransform,
            ..default()
        }).id();
        
        commands.entity(entity).push_children(&[rectgraphic]);
    }
}

fn query_system(
    query_names: Query<&Name>,
){
    for name in &query_names{
        info!("\n\nENTITY NAME: {} \n\n",name.0);
    }
}


/// We separated our internal representation of our Position from Bevy's Transform.
/// We apply our internal state to Bevy’s Transform in one place. This way our BallBundle can have its own representation of
/// Position and we better model our own game logic rather than the logic of Bevy’s renderer.
fn project_positions(
    mut positionables: Query<(&mut Transform, &Position)>
){
    for (mut transform, position) in &mut positionables {
        transform.translation = position.0;
    }
}

fn setup(
    mut commands: Commands,
    // meshes: ResMut<Assets<Mesh>>,
    // materials: ResMut<Assets<ColorMaterial>>,
    // mut gizmos: Gizmos,
    //loader: Res<AssetServer>,
) {
    commands.spawn(Camera2dBundle::default());

    //let gensize: f32 = 50.0;
    
    //add_circles(&mut commands, &mut meshes, &mut materials, Name("redcircle".to_string()), RED,gensize, Position(Vec3::new(-gensize*2.,200.,0.)));

    //add_circles(&mut commands, &mut meshes, &mut materials, Name("goldcircle".to_string()), GOLD,gensize, Position(Vec3::new(gensize*2.,0., 0.)));

    commands.spawn(SpatialBundle::default())
        .insert(Name("startpt".to_string()))
        .insert(Position(Vec3::new(-100.,0., 0.)));

    commands.spawn(SpatialBundle::default())
        .insert(Name("endpt".to_string()))
        .insert(Position(Vec3::new(100.,100., 0.)));

    // commands.spawn((
    //     MaterialMesh2dBundle{
    //     mesh: meshes.add(Segment2d::from_points(Vec2::new(-100.,0.), Vec2::new(100.,100.))).into(),
    //     material: materials.add(RED),
    //     ..default()
    //     }
    // ));
    
    // commands.spawn((
    //     SpatialBundle::Default(),
    //     Shape::Line(Segment2d::from_points(Vec2::new(-100.,0.),Vec2::new(100.,100.))),
    //     Name("line".to_string()),
    // ));

    // commands.spawn(SpatialBundle::default())
    //     .insert(Shape::Line(Segment2d::from_points(Vec2::new(-100.,0.), Vec2::new(100.,100.))));


    // create base rectangle
    // use SpatialBundle to enable transform, visibility, etc.
    // see: https://bevyengine.org/learn/errors/b0004/
    commands.spawn(SpatialBundle::default())
        .insert(RectLink{
            name : Name("thinrect".to_string()),
            from_supply: FromSupply("startpt".to_string()),
            to_sink: ToSink("endpt".to_string()),
            basewidth: Width(5.),
            ..default()
        });
    
}


// fn add_circles(
//     commands: &mut Commands,
//     meshes: &mut ResMut<Assets<Mesh>>,
//     materials: &mut ResMut<Assets<ColorMaterial>>,
//     shapename: Name,
//     shapecolor: Color,
//     shapesize: f32,
//     position: Position,
// ){
//     commands
//         .spawn(MaterialMesh2dBundle{
//             mesh:Mesh2dHandle(meshes.add(Circle::new(shapesize))),
//             material: materials.add(shapecolor),
//             ..default()
//         })
//         .insert(position)
//         .insert(shapename);
// }

    
#[derive(Component)]
struct Position(Vec3);
#[derive(Component)]
struct FromPos(Vec3);
#[derive(Component)]
struct ToPos(Vec3);
#[derive(Component, Debug)]
struct Name(String);
#[derive(Component)]
struct FromSupply(String);
#[derive(Component)]
struct ToSink(String);
#[derive(Component)]
struct Width(f32);

#[derive(Bundle)]
struct RectLink{
    name : Name,
    from_supply: FromSupply,
    frompos: FromPos,
    to_sink: ToSink,
    topos: ToPos,
    basewidth: Width,
}

// implement default (zero) initialization
// see: https://bevy-cheatbook.github.io/programming/bundle.html
impl Default for RectLink{
    fn default() -> Self {
        Self {
            name: Name("temp".to_string()),
            from_supply: FromSupply("temp".to_string()),
            frompos: FromPos(Vec3::new(0.,0.,0.)),
            to_sink: ToSink("temp".to_string()),
            topos: ToPos(Vec3::new(0.,0.,0.)),
            basewidth: Width(0.),
        }
    }
}


// COLOURS //
// https://docs.rs/bevy_render/latest/src/bevy_render/color/mod.rs.html#60-1275
    /// <div style="background-color:rgb(94%, 97%, 100%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const ALICE_BLUE: Color = Color::rgb(0.94, 0.97, 1.0);
    /// <div style="background-color:rgb(98%, 92%, 84%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const ANTIQUE_WHITE: Color = Color::rgb(0.98, 0.92, 0.84);
    /// <div style="background-color:rgb(49%, 100%, 83%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const AQUAMARINE: Color = Color::rgb(0.49, 1.0, 0.83);
    /// <div style="background-color:rgb(94%, 100%, 100%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const AZURE: Color = Color::rgb(0.94, 1.0, 1.0);
    /// <div style="background-color:rgb(96%, 96%, 86%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const BEIGE: Color = Color::rgb(0.96, 0.96, 0.86);
    /// <div style="background-color:rgb(100%, 89%, 77%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const BISQUE: Color = Color::rgb(1.0, 0.89, 0.77);
    /// <div style="background-color:rgb(0%, 0%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const BLACK: Color = Color::rgb(0.0, 0.0, 0.0);
    /// <div style="background-color:rgb(0%, 0%, 100%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const BLUE: Color = Color::rgb(0.0, 0.0, 1.0);
    /// <div style="background-color:rgb(86%, 8%, 24%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const CRIMSON: Color = Color::rgb(0.86, 0.08, 0.24);
    /// <div style="background-color:rgb(0%, 100%, 100%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const CYAN: Color = Color::rgb(0.0, 1.0, 1.0);
    /// <div style="background-color:rgb(25%, 25%, 25%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const DARK_GRAY: Color = Color::rgb(0.25, 0.25, 0.25);
    /// <div style="background-color:rgb(0%, 50%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const DARK_GREEN: Color = Color::rgb(0.0, 0.5, 0.0);
    /// <div style="background-color:rgb(100%, 0%, 100%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const FUCHSIA: Color = Color::rgb(1.0, 0.0, 1.0);
    /// <div style="background-color:rgb(100%, 84%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const GOLD: Color = Color::rgb(1.0, 0.84, 0.0);
    /// <div style="background-color:rgb(50%, 50%, 50%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const GRAY: Color = Color::rgb(0.5, 0.5, 0.5);
    /// <div style="background-color:rgb(0%, 100%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const GREEN: Color = Color::rgb(0.0, 1.0, 0.0);
    /// <div style="background-color:rgb(28%, 0%, 51%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const INDIGO: Color = Color::rgb(0.29, 0.0, 0.51);
    /// <div style="background-color:rgb(20%, 80%, 20%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const LIME_GREEN: Color = Color::rgb(0.2, 0.8, 0.2);
    /// <div style="background-color:rgb(50%, 0%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const MAROON: Color = Color::rgb(0.5, 0.0, 0.0);
    /// <div style="background-color:rgb(10%, 10%, 44%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const MIDNIGHT_BLUE: Color = Color::rgb(0.1, 0.1, 0.44);
    /// <div style="background-color:rgb(0%, 0%, 50%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const NAVY: Color = Color::rgb(0.0, 0.0, 0.5);
    /// <div style="background-color:rgba(0%, 0%, 0%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    #[doc(alias = "transparent")]
    pub const NONE: Color = Color::rgba(0.0, 0.0, 0.0, 0.0);
    /// <div style="background-color:rgb(50%, 50%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const OLIVE: Color = Color::rgb(0.5, 0.5, 0.0);
    /// <div style="background-color:rgb(100%, 65%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const ORANGE: Color = Color::rgb(1.0, 0.65, 0.0);
    /// <div style="background-color:rgb(100%, 27%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const ORANGE_RED: Color = Color::rgb(1.0, 0.27, 0.0);
    /// <div style="background-color:rgb(100%, 8%, 57%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const PINK: Color = Color::rgb(1.0, 0.08, 0.58);
    /// <div style="background-color:rgb(50%, 0%, 50%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const PURPLE: Color = Color::rgb(0.5, 0.0, 0.5);
    /// <div style="background-color:rgb(100%, 0%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const RED: Color = Color::rgb(1.0, 0.0, 0.0);
    /// <div style="background-color:rgb(98%, 50%, 45%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const SALMON: Color = Color::rgb(0.98, 0.5, 0.45);
    /// <div style="background-color:rgb(18%, 55%, 34%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const SEA_GREEN: Color = Color::rgb(0.18, 0.55, 0.34);
    /// <div style="background-color:rgb(75%, 75%, 75%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const SILVER: Color = Color::rgb(0.75, 0.75, 0.75);
    /// <div style="background-color:rgb(0%, 50%, 50%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const TEAL: Color = Color::rgb(0.0, 0.5, 0.5);
    /// <div style="background-color:rgb(100%, 39%, 28%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const TOMATO: Color = Color::rgb(1.0, 0.39, 0.28);
    /// <div style="background-color:rgb(25%, 88%, 82%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const TURQUOISE: Color = Color::rgb(0.25, 0.88, 0.82);
    /// <div style="background-color:rgb(93%, 51%, 93%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const VIOLET: Color = Color::rgb(0.93, 0.51, 0.93);
    /// <div style="background-color:rgb(100%, 100%, 100%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const WHITE: Color = Color::rgb(1.0, 1.0, 1.0);
    /// <div style="background-color:rgb(100%, 100%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const YELLOW: Color = Color::rgb(1.0, 1.0, 0.0);
    /// <div style="background-color:rgb(60%, 80%, 20%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const YELLOW_GREEN: Color = Color::rgb(0.6, 0.8, 0.2);

// #[derive(Component, Debug)]
// struct Name(String);

// #[derive(Bundle, Debug)]
// struct Entity{
//     name: String,
//     position: Vec3,
//     #[bundle]
//     circle: ShapeCircle    
// }

// #[derive(Component, Debug)]
// struct Position(Vec3);

// #[derive(Bundle)]
// struct ShapeCircle{
//     name: String,
//     size: f32,
//     color: Color,
// }


// examples
    // let color1 = Color::rgb(0.0,0.0,0.5);
    // commands.spawn(MaterialMesh2dBundle{
    //     mesh:Mesh2dHandle(meshes.add(RegularPolygon::new(width, 6))),
    //     material: materials.add(color1),
    //     transform: Transform::from_xyz(-width,0.,0.),
    //     ..default()
    // });

    // let color2 = Color::rgb(1.0,0.0,0.0);
    // commands.spawn(MaterialMesh2dBundle{
    //     mesh:Mesh2dHandle(meshes.add(RegularPolygon::new(width, 5))),
    //     material: materials.add(color2),
    //     transform: Transform::from_xyz(width,0.,1.),
    //     ..default()
    // });
