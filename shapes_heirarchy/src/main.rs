// https://bevy-cheatbook.github.io/fundamentals/hierarchy.html

use bevy::{
    prelude::*,
    sprite::{MaterialMesh2dBundle, Mesh2dHandle},
    ui::update,
};

use bevy_mod_picking::prelude::*;


fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugins(DefaultPickingPlugins)
        .add_systems(Startup, (setup, query_system.after(setup)))
        .add_systems(Update, project_positions)
        .run();
}


fn query_system(
    mut query_names: Query<(&Name, &mut Position)>,
){
    
    for (name, mut pos) in &mut query_names{
        info!("\nENTITY NAME: {} \n",name.0);
        if name.0 == "greentriangle".to_string(){
            info!("found triangle");
            *pos = Position(Vec2::new(-100.0,100.0));
        };
    }
}

/// We separated our internal representation of our Position from Bevy's Transform.
/// We apply our internal state to Bevy’s Transform in one place. This way our BallBundle can have its own representation of
/// Position and we better model our own game logic rather than the logic of Bevy’s renderer.
fn project_positions(
    mut positionables: Query<(&mut Transform, &Position)>,
){
    //triangletop.position = (Position(Vec2::new(0.,-gensize*2.0)));
    for (mut transform, position) in &mut positionables {
        transform.translation = position.0.extend(0.);
    }
}

fn setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    commands.spawn(Camera2dBundle::default());

    let gensize: f32 = 50.0;

    let circle1 = commands.spawn(MaterialMesh2dBundle{
        mesh:Mesh2dHandle(meshes.add(Circle::new(gensize))),
        material: materials.add(RED),
        ..default()
    })
        .insert(Position(Vec2::new(-gensize*2.,0.)))
        .insert(Name("redcircle".to_string()))
        .id();
    let circle2 = commands.spawn((MaterialMesh2dBundle{
        mesh:Mesh2dHandle(meshes.add(Circle::new(gensize))),
        material: materials.add(GOLD),
        ..default()
    },
                                  PickableBundle::default(),
    ))
        .insert(Position(Vec2::new(gensize*2.,0.)))
        .insert(Name("goldcircle".to_string()))
        .id();
    let triangletop = commands.spawn(MaterialMesh2dBundle{
        mesh:Mesh2dHandle(meshes.add(Triangle2d::new(
            Vec2::Y * gensize,
            Vec2::new(-gensize, -gensize),
            Vec2::new(gensize, -gensize),
        ))),
        material: materials.add(GREEN),
        ..default()
    })
        .insert(Position(Vec2::new(0.,2.0*gensize)))
        .insert(Name("greentriangle".to_string()))
        .id();

    commands.entity(triangletop).push_children(&[circle1,circle2]);
    commands.entity(circle2).insert(Position(Vec2::new(100.,100.)));
    //commands.entity(triangletop).try_insert(Position(Vec2::new(-2.0*gensize,2.0*gensize)));


    
    // let circle3 = commands.spawn(MaterialMesh2dBundle{
    //     mesh:Mesh2dHandle(meshes.add(Circle::new(gensize))),
    //     material: materials.add(PINK),
    //     ..default()
    // })
    //     .insert(Position(Vec2::new(-gensize*2.,0.)))
    //     .insert(Name("pinkcircle".to_string()))
    //     .id();
    // let circle4 = commands.spawn(MaterialMesh2dBundle{
    //     mesh:Mesh2dHandle(meshes.add(Circle::new(gensize))),
    //     material: materials.add(PURPLE),
    //     ..default()
    // })
    //     .insert(Position(Vec2::new(gensize*2.,0.)))
    //     .insert(Name("purplecircle".to_string()))
    //     .id();
    
    let trianglebot = commands.spawn(MaterialMesh2dBundle{
        mesh:Mesh2dHandle(meshes.add(Triangle2d::new(
            Vec2::Y * gensize,
            Vec2::new(-gensize, -gensize),
            Vec2::new(gensize, -gensize),
        ))),
        material: materials.add(MAROON),
        ..default()
    })
        .insert(Position(Vec2::new(0.,-2.0*gensize)))
        .insert(Name("maroontriangle".to_string()))
        .with_children(|parent| {
            parent.spawn(MaterialMesh2dBundle{
                mesh:Mesh2dHandle(meshes.add(Circle::new(gensize))),
                material: materials.add(PINK),
                ..default()
            })
                .insert(Position(Vec2::new(-gensize*2.,2.0*gensize)))
                .insert(Name("pinkcircle".to_string()));
            parent.spawn((MaterialMesh2dBundle{
                mesh:Mesh2dHandle(meshes.add(Circle::new(gensize))),
                material: materials.add(PURPLE),
                ..default()
            },
                          PickableBundle::default(),
            ))
                .insert(Position(Vec2::new(gensize*2.,2.0*gensize)))
                .insert(Name("purplecircle".to_string()));
        })
        .id();


        
    //add_circles(&mut commands, &mut meshes, &mut materials, Name("redcircle".to_string()), RED,gensize, Position(Vec2::new(-gensize*2.,0.)));

    //add_circles(&mut commands, &mut meshes, &mut materials, Name("goldcircle".to_string()), GOLD,gensize, Position(Vec2::new(gensize*2.,0.)));
    
}


// fn add_circles(
//     commands: &mut Commands,
//     meshes: &mut ResMut<Assets<Mesh>>,
//     materials: &mut ResMut<Assets<ColorMaterial>>,
//     shapename: Name,
//     shapecolor: Color,
//     shapesize: f32,
//     position: Position,
// ){
//     commands
//         .spawn(MaterialMesh2dBundle{
//             mesh:Mesh2dHandle(meshes.add(Circle::new(shapesize))),
//             material: materials.add(shapecolor),
//             ..default()
//         })
//         .insert(position)
//         .insert(shapename);
// }

#[derive(Component)]
struct Position(Vec2);
#[derive(Component, Debug)]
struct Name(String);


// COLOURS //
    pub const FUCHSIA: Color = Color::rgb(1.0, 0.0, 1.0);
    /// <div style="background-color:rgb(100%, 84%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const GOLD: Color = Color::rgb(1.0, 0.84, 0.0);
    /// <div style="background-color:rgb(50%, 50%, 50%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const GRAY: Color = Color::rgb(0.5, 0.5, 0.5);
    /// <div style="background-color:rgb(0%, 100%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const GREEN: Color = Color::rgb(0.0, 1.0, 0.0);
    /// <div style="background-color:rgb(28%, 0%, 51%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const INDIGO: Color = Color::rgb(0.29, 0.0, 0.51);
    /// <div style="background-color:rgb(20%, 80%, 20%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const LIME_GREEN: Color = Color::rgb(0.2, 0.8, 0.2);
    /// <div style="background-color:rgb(50%, 0%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const MAROON: Color = Color::rgb(0.5, 0.0, 0.0);
    /// <div style="background-color:rgb(10%, 10%, 44%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const MIDNIGHT_BLUE: Color = Color::rgb(0.1, 0.1, 0.44);
    /// <div style="background-color:rgb(0%, 0%, 50%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const NAVY: Color = Color::rgb(0.0, 0.0, 0.5);
    /// <div style="background-color:rgba(0%, 0%, 0%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    #[doc(alias = "transparent")]
    pub const NONE: Color = Color::rgba(0.0, 0.0, 0.0, 0.0);
    /// <div style="background-color:rgb(50%, 50%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const OLIVE: Color = Color::rgb(0.5, 0.5, 0.0);
    /// <div style="background-color:rgb(100%, 65%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const ORANGE: Color = Color::rgb(1.0, 0.65, 0.0);
    /// <div style="background-color:rgb(100%, 27%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const ORANGE_RED: Color = Color::rgb(1.0, 0.27, 0.0);
    /// <div style="background-color:rgb(100%, 8%, 57%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const PINK: Color = Color::rgb(1.0, 0.08, 0.58);
    /// <div style="background-color:rgb(50%, 0%, 50%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const PURPLE: Color = Color::rgb(0.5, 0.0, 0.5);
    /// <div style="background-color:rgb(100%, 0%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const RED: Color = Color::rgb(1.0, 0.0, 0.0);

// #[derive(Component, Debug)]
// struct Name(String);

// #[derive(Bundle, Debug)]
// struct Entity{
//     name: String,
//     position: Vec3,
//     #[bundle]
//     circle: ShapeCircle    
// }

// #[derive(Component, Debug)]
// struct Position(Vec3);

// #[derive(Bundle)]
// struct ShapeCircle{
//     name: String,
//     size: f32,
//     color: Color,
// }


// examples
    // let color1 = Color::rgb(0.0,0.0,0.5);
    // commands.spawn(MaterialMesh2dBundle{
    //     mesh:Mesh2dHandle(meshes.add(RegularPolygon::new(width, 6))),
    //     material: materials.add(color1),
    //     transform: Transform::from_xyz(-width,0.,0.),
    //     ..default()
    // });

    // let color2 = Color::rgb(1.0,0.0,0.0);
    // commands.spawn(MaterialMesh2dBundle{
    //     mesh:Mesh2dHandle(meshes.add(RegularPolygon::new(width, 5))),
    //     material: materials.add(color2),
    //     transform: Transform::from_xyz(width,0.,1.),
    //     ..default()
    // });
