use bevy::{
    prelude::*,
    sprite::{MaterialMesh2dBundle, Mesh2dHandle}, ui::update,
};

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_systems(Startup, (setup, query_system.after(setup)))
        .add_systems(Update, project_positions)
        .run();
}


fn query_system(
    query_names: Query<&Name>,
){
    for name in &query_names{
        println!("\n\nENTITY NAME: {} \n\n",name.0);
    }
}


/// We separated our internal representation of our Position from Bevy's Transform.
/// We apply our internal state to Bevy’s Transform in one place. This way our BallBundle can have its own representation of
/// Position and we better model our own game logic rather than the logic of Bevy’s renderer.
fn project_positions(
    mut positionables: Query<(&mut Transform, &Position)>
){
    for (mut transform, position) in &mut positionables {
        transform.translation = position.0.extend(0.);
    }
}

fn setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    commands.spawn(Camera2dBundle::default());

    let gensize: f32 = 50.0;
    
    add_circles(&mut commands, &mut meshes, &mut materials, Name("redcircle".to_string()), RED,gensize, Position(Vec2::new(-gensize*2.,0.)));

    add_circles(&mut commands, &mut meshes, &mut materials, Name("goldcircle".to_string()), GOLD,gensize, Position(Vec2::new(gensize*2.,0.)));
    
}


fn add_circles(
    commands: &mut Commands,
    meshes: &mut ResMut<Assets<Mesh>>,
    materials: &mut ResMut<Assets<ColorMaterial>>,
    shapename: Name,
    shapecolor: Color,
    shapesize: f32,
    position: Position,
){
    commands
        .spawn(MaterialMesh2dBundle{
            mesh:Mesh2dHandle(meshes.add(Circle::new(shapesize))),
            material: materials.add(shapecolor),
            ..default()
        })
        .insert(position)
        .insert(shapename);
}

#[derive(Component)]
struct Position(Vec2);
#[derive(Component, Debug)]
struct Name(String);


// COLOURS //
    pub const FUCHSIA: Color = Color::rgb(1.0, 0.0, 1.0);
    /// <div style="background-color:rgb(100%, 84%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const GOLD: Color = Color::rgb(1.0, 0.84, 0.0);
    /// <div style="background-color:rgb(50%, 50%, 50%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const GRAY: Color = Color::rgb(0.5, 0.5, 0.5);
    /// <div style="background-color:rgb(0%, 100%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const GREEN: Color = Color::rgb(0.0, 1.0, 0.0);
    /// <div style="background-color:rgb(28%, 0%, 51%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const INDIGO: Color = Color::rgb(0.29, 0.0, 0.51);
    /// <div style="background-color:rgb(20%, 80%, 20%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const LIME_GREEN: Color = Color::rgb(0.2, 0.8, 0.2);
    /// <div style="background-color:rgb(50%, 0%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const MAROON: Color = Color::rgb(0.5, 0.0, 0.0);
    /// <div style="background-color:rgb(10%, 10%, 44%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const MIDNIGHT_BLUE: Color = Color::rgb(0.1, 0.1, 0.44);
    /// <div style="background-color:rgb(0%, 0%, 50%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const NAVY: Color = Color::rgb(0.0, 0.0, 0.5);
    /// <div style="background-color:rgba(0%, 0%, 0%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    #[doc(alias = "transparent")]
    pub const NONE: Color = Color::rgba(0.0, 0.0, 0.0, 0.0);
    /// <div style="background-color:rgb(50%, 50%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const OLIVE: Color = Color::rgb(0.5, 0.5, 0.0);
    /// <div style="background-color:rgb(100%, 65%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const ORANGE: Color = Color::rgb(1.0, 0.65, 0.0);
    /// <div style="background-color:rgb(100%, 27%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const ORANGE_RED: Color = Color::rgb(1.0, 0.27, 0.0);
    /// <div style="background-color:rgb(100%, 8%, 57%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const PINK: Color = Color::rgb(1.0, 0.08, 0.58);
    /// <div style="background-color:rgb(50%, 0%, 50%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const PURPLE: Color = Color::rgb(0.5, 0.0, 0.5);
    /// <div style="background-color:rgb(100%, 0%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const RED: Color = Color::rgb(1.0, 0.0, 0.0);

// #[derive(Component, Debug)]
// struct Name(String);

// #[derive(Bundle, Debug)]
// struct Entity{
//     name: String,
//     position: Vec3,
//     #[bundle]
//     circle: ShapeCircle    
// }

// #[derive(Component, Debug)]
// struct Position(Vec3);

// #[derive(Bundle)]
// struct ShapeCircle{
//     name: String,
//     size: f32,
//     color: Color,
// }


// examples
    // let color1 = Color::rgb(0.0,0.0,0.5);
    // commands.spawn(MaterialMesh2dBundle{
    //     mesh:Mesh2dHandle(meshes.add(RegularPolygon::new(width, 6))),
    //     material: materials.add(color1),
    //     transform: Transform::from_xyz(-width,0.,0.),
    //     ..default()
    // });

    // let color2 = Color::rgb(1.0,0.0,0.0);
    // commands.spawn(MaterialMesh2dBundle{
    //     mesh:Mesh2dHandle(meshes.add(RegularPolygon::new(width, 5))),
    //     material: materials.add(color2),
    //     transform: Transform::from_xyz(width,0.,1.),
    //     ..default()
    // });
