use bevy::{
    prelude::*,
    color::palettes::basic::{PURPLE},
    sprite::{MaterialMesh2dBundle, Mesh2dHandle},
};

//need to go one extra level up 
use super::super::{GameDifficulty};
use super::game::*;

pub fn game_level_1(
    mut commands: Commands,
    //game_difficulty: Res<GameDifficulty>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    //game_level: Res<GameLevel>,
    //timer: Res<GameTimer>,
    dcolor: Res<DifficultyColor>
) {
    let width = 50.0;
    //info!("{:#?}",dcolor.0);
    //OnGameScreen
    commands.spawn((PbrBundle::default(), OnGameScreen))
        .with_children(|parent|{
            parent.spawn(MaterialMesh2dBundle{
                mesh:Mesh2dHandle(meshes.add(RegularPolygon::new(width, 6))),
                material: materials.add(dcolor.0),
                transform: Transform::from_xyz(-width,0.,0.),
                ..default()
            });
        });
}

pub fn game_level_2(
    mut commands: Commands,
    //game_difficulty: Res<GameDifficulty>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    //game_level: Res<GameLevel>,
    //timer: Res<GameTimer>,
    dcolor: Res<DifficultyColor>
) {
    let width = 50.0;
    //OnGameScreen
    commands.spawn((PbrBundle::default(), OnGameScreen))
        .with_children(|parent|{
            parent.spawn(MaterialMesh2dBundle{
                mesh:Mesh2dHandle(meshes.add(RegularPolygon::new(width, 6))),
                material: materials.add(dcolor.0),
                transform: Transform::from_xyz(-width,0.,0.),
                ..default()
            });
            parent.spawn(MaterialMesh2dBundle{
                mesh:Mesh2dHandle(meshes.add(RegularPolygon::new(width, 5))),
                material: materials.add(dcolor.0),
                transform: Transform::from_xyz(width,0.,0.),
                ..default()
            });
        });
}

pub fn game_level_3(
    mut commands: Commands,
    game_difficulty: Res<GameDifficulty>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    //game_level: Res<GameLevel>,
    //timer: Res<GameTimer>,
    dcolor: Res<DifficultyColor>
) {
    let width = 50.0;
    //OnGameScreen
    commands.spawn((PbrBundle::default(), OnGameScreen))
        .with_children(|parent|{
            parent.spawn(MaterialMesh2dBundle{
                mesh:Mesh2dHandle(meshes.add(RegularPolygon::new(width, 6))),
                material: materials.add(dcolor.0),
                transform: Transform::from_xyz(-width,0.,0.),
                ..default()
            });
            parent.spawn(MaterialMesh2dBundle{
                mesh:Mesh2dHandle(meshes.add(RegularPolygon::new(width, 5))),
                material: materials.add(dcolor.0),
                transform: Transform::from_xyz(width,0.,0.),
                ..default()
            });
            parent.spawn(MaterialMesh2dBundle{
                mesh:Mesh2dHandle(meshes.add(RegularPolygon::new(width, 4))),
                material: materials.add(dcolor.0),
                transform: Transform::from_xyz(0.,2.*width,0.),
                ..default()
            });
            if *game_difficulty == GameDifficulty::Difficult {
                parent.spawn(MaterialMesh2dBundle{
                    mesh:Mesh2dHandle(meshes.add(RegularPolygon::new(width, 3))),
                    material: materials.add(Color::from(PURPLE)),
                    transform: Transform::from_xyz(0.,-2.*width,0.),
                    ..default()
                }); 
            }
        });
}

