use bevy::{
    color::palettes::basic::{BLUE, LIME},
    prelude::*,
};
//need to go one extra level up 
use super::super::{GameDifficulty, GameLevel, TEXT_COLOR};

use super::game::{OnGameScreen};

#[derive(Component)]
pub struct TimerText;

#[derive(Resource, Debug, Deref, DerefMut)]
pub struct GameTimer(Timer);

pub fn game_ui_setup(
    mut commands: Commands,
    game_difficulty: Res<GameDifficulty>,
    game_level: Res<GameLevel>,
    //timer: Res<GameTimer>,
) {
    // set timer for game
    commands.insert_resource(GameTimer(Timer::from_seconds(3.0, TimerMode::Once)));
    // load game ui components
    commands
        .spawn((
            NodeBundle {
                style: Style {
                    width: Val::Percent(100.0),
                    height: Val::Percent(100.0),
                    // center children
                    align_items: AlignItems::Center,
                    justify_content: JustifyContent::Center,
                    ..default()
                },
                ..default()
            },
            OnGameScreen,
        ))
        .with_children(|parent| {
            // First create a `NodeBundle` for centering what we want to display
            parent
                .spawn(NodeBundle {
                    style: Style {
                        // This will display its children in a column, from top to bottom
                        flex_direction: FlexDirection::Row,
                        // `align_items` will align children on the cross axis. Here the main axis is
                        // vertical (column), so the cross axis is horizontal. This will center the
                        // children
                        align_items: AlignItems::Center,
                        position_type: PositionType::Absolute,
                        top: Val::Px(5.0),
                        right: Val::Px(5.0),
                        ..default()
                    },
                    background_color: Color::BLACK.into(),
                    ..default()
                })
                .with_children(|parent| {
                    // Display two lines of text, the second one with the current settings
                    parent.spawn(
                        TextBundle::from_sections([
                            TextSection::new(
                                "Waiting: ",
                                TextStyle {
                                    font_size: 40.0,
                                    ..default()
                                },
                            ),
                            TextSection::new(
                                "0",
                                TextStyle {
                                    font_size: 40.0,
                                    ..default()
                                },
                            ),
                        ])        )
                        .insert(TimerText);        
                    parent.spawn(
                        TextBundle::from_sections([
                            TextSection::new(
                                format!("Difficulty: {:?}", *game_difficulty),
                                TextStyle {
                                    font_size: 20.0,
                                    color: BLUE.into(),
                                    ..default()
                                },
                            ),
                            TextSection::new(
                                " - ",
                                TextStyle {
                                    font_size: 20.0,
                                    color: TEXT_COLOR,
                                    ..default()
                                },
                            ),
                            TextSection::new(
                                format!("Level: {:?}", *game_level),
                                TextStyle {
                                    font_size: 20.0,
                                    color: LIME.into(),
                                    ..default()
                                },
                            ),
                        ])
                        .with_style(Style {
                            margin: UiRect::all(Val::Px(50.0)),
                            ..default()
                        }),
                    );
                });
        });    
}

pub fn text_update_timer(
    //time: Res<Time<Virtual>>,
    gtimer: ResMut<GameTimer>,
    //vtime: Res<Virtual>,
    mut query: Query<&mut Text, With<TimerText>>,
) {
    //let time_elapsed = gtimer.elapsed_secs();
    let time_remain = gtimer.remaining_secs();
//    let mut stopwatch = Stopwatch::new();
//    let seconds_counted = stopwatch.elapsed_secs();
    //let gametime = time.delta();
    for mut text in &mut query {
        // Update the value of the second section
        text.sections[1].value = format!("{:.0?}",time_remain);
    }
}
