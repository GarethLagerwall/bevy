//mod game {
use bevy::{
    prelude::*,
    color::palettes::basic::WHITE,
};
//need to go one extra level up 
use super::super::{despawn_screen, GameState, GameLevel, GameDifficulty};
use super::game_ui::*;
use super::game_levels::*;
use super::game_difficulty::*;

// Tag component used to tag entities added on the game screen
#[derive(Component)]
pub struct OnGameScreen;
#[derive(Resource, Debug, Component, PartialEq, Clone, Copy)]
pub struct DifficultyColor(pub Color);


// This plugin will contain the game. In this case, it's just be a screen that will
// display the current settings for 5 seconds before returning to the menu
pub fn game_plugin(app: &mut App) {
    app.insert_resource(DifficultyColor(Color::from(WHITE)));
    app.add_systems(OnEnter(GameState::Game),
                    (game_ui_setup,
                     game_difficulty_set_color,
                     game_level_1.run_if(run_if_level_1).after(game_difficulty_set_color),
                     game_level_2.run_if(run_if_level_2).after(game_difficulty_set_color),
                     game_level_3.run_if(run_if_level_3).after(game_difficulty_set_color),
                    )
                    //game_level_1)
    )
        .add_systems(Update,
                     (game_update.run_if(in_state(GameState::Game)),
                      text_update_timer.run_if(in_state(GameState::Game)))
        )
        .add_systems(OnExit(GameState::Game), despawn_screen::<OnGameScreen>);
}

// Tick the timer, and change state when finished
fn game_update(
    time: Res<Time>,
    mut game_state: ResMut<NextState<GameState>>,
    mut timer: ResMut<GameTimer>,
) {
    if timer.tick(time.delta()).finished() {
        game_state.set(GameState::Menu);
    }
}
//}

fn run_if_level_1(
    //needs to be read only to work
    level: Res<GameLevel>
) -> bool
{
    *level == GameLevel::Level1
}
fn run_if_level_2(
    //needs to be read only to work
    level: Res<GameLevel>
) -> bool
{
    *level == GameLevel::Level2
}
fn run_if_level_3(
    //needs to be read only to work
    level: Res<GameLevel>
) -> bool
{
    *level == GameLevel::Level3
}
