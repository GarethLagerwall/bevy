use bevy::{
    prelude::*,
    color::palettes::basic::{RED,GREEN,YELLOW},
    //sprite::{MaterialMesh2dBundle, Mesh2dHandle},
};

//need to go one extra level up 
use super::super::GameDifficulty;
use super::game::*;


pub fn game_difficulty_set_color(
    //mut commands: Commands,
    game_difficulty: Res<GameDifficulty>,
    //mut meshes: ResMut<Assets<Mesh>>,
    //mut materials: ResMut<Assets<ColorMaterial>>,
    //game_level: Res<GameLevel>,
    //timer: Res<GameTimer>,
    mut color: ResMut<DifficultyColor>
) {
    match *game_difficulty {
        GameDifficulty::Easy => {
            info!("easy setting");
            *color=DifficultyColor(Color::from(GREEN));
        }
        GameDifficulty::Medium => {
            info!("medium setting");
            *color=DifficultyColor(Color::from(YELLOW));
        }
        GameDifficulty::Difficult => {
            info!("difficult setting");
            *color=DifficultyColor(Color::from(RED));
        }
    }
}

