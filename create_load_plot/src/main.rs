use bevy::{
    prelude::*,
    sprite::{SpriteBundle},
};
use plotters::prelude::*;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_systems(Startup, (setup_camera, setup_render.after(setup_camera)))
        .run();
}

fn setup_render(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
) {
    commands.spawn(SpriteBundle {
        texture: asset_server.load("test/0.png"),
        ..default()
    });
}

fn setup_plot() -> Result<(), Box<dyn std::error::Error>> {
    let root = BitMapBackend::new("./assets/test/0.png", (640, 480)).into_drawing_area();
    //need to distinguish between Bevy colors and plotters colors
    root.fill(&plotters::style::colors::WHITE)?;
    let mut chart = ChartBuilder::on(&root)
        .caption("y=x^2", ("sans-serif", 50).into_font())
        .margin(5)
        .x_label_area_size(30)
        .y_label_area_size(30)
        .build_cartesian_2d(-1f32..1f32, -0.1f32..1f32)?;

    chart.configure_mesh().draw()?;

    chart
        .draw_series(LineSeries::new(
            (-50..=50).map(|x| x as f32 / 50.0).map(|x| (x, x * x)),
            &plotters::style::colors::RED,
        ))?
        .label("y = x^2")
        .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], &plotters::style::colors::RED));

    chart
        .configure_series_labels()
        .background_style(&plotters::style::colors::WHITE)
        .border_style(&plotters::style::colors::BLACK)
        .draw()?;

    root.present()?;

    Ok(())
}

fn setup_camera(
    mut commands: Commands,
) {
    commands.spawn(Camera2dBundle::default());
    let _ = setup_plot();
}

