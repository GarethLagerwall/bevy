// https://bevy-cheatbook.github.io/fundamentals/hierarchy.html

use bevy::{
    prelude::*,
    sprite::{MaterialMesh2dBundle, Mesh2dHandle}, ui::update,
};

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_systems(Startup,
                     (setup,
                      query_system.after(setup),
                      rotate_system.after(setup),
                      norotate_system.after(rotate_system)))
        .add_systems(Update, project_positions)
        .run();
}

fn rotate_system(
    mut query_t: Query<&mut Transform, With<RotMark>>,
    //mut query_nt: Query<&mut Transform, With<NoRotMark>>,
){
    // To rotate an object in 2D (Z-axis rotation) by 30°
    // (angles are in radians! must convert from degrees!)
    // https://bevy-cheatbook.github.io/fundamentals/transforms.html
    let rot2d = Quat::from_rotation_z((-30.0_f32).to_radians());
    for mut trans in &mut query_t{
        info!("\n\nRotating\n\n");
        trans.rotation = rot2d;
    }
}


/// rotation does perform a bit of translate
/// need to perform rotation on grandparent
/// then undo this to keep text in scope and horizontally-aligned
fn norotate_system(
    //mut query_t: Query<&mut Transform, With<RotMark>>,
    mut query_nt: Query<&mut Transform, With<NoRotMark>>,
){
    // To rotate an object in 2D (Z-axis rotation) by 30°
    // (angles are in radians! must convert from degrees!)
    // https://bevy-cheatbook.github.io/fundamentals/transforms.html    
    let norot2d = Quat::from_rotation_z((30.0_f32).to_radians());
    for mut trans in &mut query_nt{
        info!("\n\nDe-Rotating\n\n");
        trans.rotation = norot2d;
    }    
}


fn query_system(
    mut query_names: Query<(&Name, &mut Position)>,
){
    
    for (name, mut pos) in &mut query_names{
        info!("\nENTITY NAME: {} \n",name.0);
        if name.0 == "greentriangle".to_string(){
            info!("found triangle");
            *pos = Position(Vec2::new(-100.0,100.0));
        };
    }
}

/// We separated our internal representation of our Position from Bevy's Transform.
/// We apply our internal state to Bevy’s Transform in one place. This way our BallBundle can have its own representation of
/// Position and we better model our own game logic rather than the logic of Bevy’s renderer.
fn project_positions(
    mut positionables: Query<(&mut Transform, &Position)>,
){
    //triangletop.position = (Position(Vec2::new(0.,-gensize*2.0)));
    for (mut transform, position) in &mut positionables {
        info!("Position: {:?}",position.0);
        transform.translation = position.0.extend(0.);
    }
}

fn setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    commands.spawn(Camera2dBundle::default());

    let gensize: f32 = 50.0;

    let grandparent = commands.spawn(SpatialBundle::default())
        .insert(Name("grandp".to_string()))
        .insert(Position(Vec2::new(0.,-2.0*gensize)))
        .insert(RotMark)
        .id();

    let asstext = commands.spawn( Text2dBundle {
        text: Text::from_section("assorted\ntext",
                                 TextStyle {
                                     font_size: 20.0,
                                     ..default()
                                 })
            .with_justify(JustifyText::Center),
        ..default()
    })
        .insert(Position(Vec2::new(0.,2.0*gensize)))
        .insert(NoRotMark)
        .id();
    
    let trianglebot = commands.spawn(MaterialMesh2dBundle{
        mesh:Mesh2dHandle(meshes.add(Triangle2d::new(
            Vec2::Y * gensize,
            Vec2::new(-gensize, -gensize),
            Vec2::new(gensize, -gensize),
        ))),
        material: materials.add(MAROON),
        ..default()
    })
        .insert(Position(Vec2::new(0.,0.)))
        .insert(Name("maroontriangle".to_string()))
        //.insert(RotMark)
        .with_children(|parent| {
            parent.spawn(MaterialMesh2dBundle{
                mesh:Mesh2dHandle(meshes.add(Circle::new(gensize))),
                material: materials.add(PINK),
                ..default()
            })
                .insert(Position(Vec2::new(-gensize*2.,2.0*gensize)))
                .insert(Name("pinkcircle".to_string()));
            parent.spawn(MaterialMesh2dBundle{
                mesh:Mesh2dHandle(meshes.add(Circle::new(gensize))),
                material: materials.add(PURPLE),
                ..default()
            })
                .insert(Position(Vec2::new(gensize*2.,2.0*gensize)))
                .insert(Name("purplecircle".to_string()));
        })
        .id();

    commands.entity(grandparent).push_children(&[trianglebot,asstext]);
    //commands.entity(trianglebot).transform();

    //adding cluster with rotations etc. all at init
    commands.spawn(
        SpatialBundle{
            transform: Transform{
                rotation: Quat::from_rotation_z((30.0_f32).to_radians()),
                ..default()
            },
            ..default()
        })
        .insert(Name("grandp2".to_string()))
        .insert(Position(Vec2::new(-4.0*gensize,4.0*gensize)))
        .with_children(|parent| {
            parent.spawn(Text2dBundle {
                text: Text::from_section("More\ntext",
                                         TextStyle {
                                             font_size: 20.0,
                                             ..default()
                                         })
                    .with_justify(JustifyText::Center),
                transform: Transform{
                    //undo parent transform
                    rotation: Quat::from_rotation_z((-30.0_f32).to_radians()),
                    ..default()
                },
                ..default()
            })
                .insert(Position(Vec2::new(0.,0.)));
            parent.spawn(
                MaterialMesh2dBundle{
                    mesh:Mesh2dHandle(meshes.add(Triangle2d::new(
                        Vec2::Y * gensize,
                        Vec2::new(-gensize, -gensize),
                        Vec2::new(gensize, -gensize),
                    ))),
                    material: materials.add(GOLD),
                    ..default()
                }
            )
                .insert(Position(Vec2::new(0.,-2.0*gensize)))
                .insert(Name("goldtriangle".to_string()))
                .with_children(|build| {
                    build.spawn(MaterialMesh2dBundle{
                        mesh:Mesh2dHandle(meshes.add(Circle::new(gensize))),
                        material: materials.add(INDIGO),
                        ..default()
                    })
                        .insert(Position(Vec2::new(-gensize*2.,gensize*2.)))
                        .insert(Name("indigocircle".to_string()));
                    build.spawn(MaterialMesh2dBundle{
                        mesh:Mesh2dHandle(meshes.add(Circle::new(gensize))),
                        material: materials.add(GREEN),
                        ..default()
                    })
                        .insert(Position(Vec2::new(gensize*2.,gensize*2.)))
                        .insert(Name("greencircle".to_string()));
                });
        });
}


#[derive(Component)]
struct Position(Vec2);
#[derive(Component, Debug)]
struct Name(String);
#[derive(Component)]
struct RotMark;
#[derive(Component)]
struct NoRotMark;

// COLOURS //
    pub const FUCHSIA: Color = Color::rgb(1.0, 0.0, 1.0);
    /// <div style="background-color:rgb(100%, 84%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const GOLD: Color = Color::rgb(1.0, 0.84, 0.0);
    /// <div style="background-color:rgb(50%, 50%, 50%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const GRAY: Color = Color::rgb(0.5, 0.5, 0.5);
    /// <div style="background-color:rgb(0%, 100%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const GREEN: Color = Color::rgb(0.0, 1.0, 0.0);
    /// <div style="background-color:rgb(28%, 0%, 51%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const INDIGO: Color = Color::rgb(0.29, 0.0, 0.51);
    /// <div style="background-color:rgb(20%, 80%, 20%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const LIME_GREEN: Color = Color::rgb(0.2, 0.8, 0.2);
    /// <div style="background-color:rgb(50%, 0%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const MAROON: Color = Color::rgb(0.5, 0.0, 0.0);
    /// <div style="background-color:rgb(10%, 10%, 44%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const MIDNIGHT_BLUE: Color = Color::rgb(0.1, 0.1, 0.44);
    /// <div style="background-color:rgb(0%, 0%, 50%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const NAVY: Color = Color::rgb(0.0, 0.0, 0.5);
    /// <div style="background-color:rgba(0%, 0%, 0%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    #[doc(alias = "transparent")]
    pub const NONE: Color = Color::rgba(0.0, 0.0, 0.0, 0.0);
    /// <div style="background-color:rgb(50%, 50%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const OLIVE: Color = Color::rgb(0.5, 0.5, 0.0);
    /// <div style="background-color:rgb(100%, 65%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const ORANGE: Color = Color::rgb(1.0, 0.65, 0.0);
    /// <div style="background-color:rgb(100%, 27%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const ORANGE_RED: Color = Color::rgb(1.0, 0.27, 0.0);
    /// <div style="background-color:rgb(100%, 8%, 57%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const PINK: Color = Color::rgb(1.0, 0.08, 0.58);
    /// <div style="background-color:rgb(50%, 0%, 50%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const PURPLE: Color = Color::rgb(0.5, 0.0, 0.5);
    /// <div style="background-color:rgb(100%, 0%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const RED: Color = Color::rgb(1.0, 0.0, 0.0);
