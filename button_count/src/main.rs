//! clicking a button increases or decreases a counter

use bevy::{prelude::*, winit::WinitSettings};
//use std::io;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        // Only run the app when there is user input. This will significantly reduce CPU/GPU use.
        .insert_resource(WinitSettings::desktop_app())
        .add_systems(Startup, setup)
        .add_systems(Update, button_system)
        .run();
}

const NORMAL_BUTTON: Color = Color::rgb(0.15, 0.15, 0.15);
const HOVERED_BUTTON: Color = Color::rgb(0.25, 0.25, 0.25);
const PRESSED_BUTTON: Color = Color::rgb(0.35, 0.75, 0.35);

// A unit struct to help identify the changing counter component
#[derive(Component)]
struct CounterText;
// A unit struct to help identify the add button component
#[derive(Component)]
struct StructAdditionButton;
// A unit struct to help identify the subtract button component
#[derive(Component)]
struct StructSubtractButton;



fn button_system(
    mut interaction_query: Query<
        (
            &Interaction,
            &mut BackgroundColor,
            &mut BorderColor,
            //&Children,
            (Option<&StructAdditionButton>,Option<&StructSubtractButton>),
        ),
        (Changed<Interaction>, With<Button>),
    >,
    mut text_query: Query<&mut Text, With<CounterText>>,
) {
    for (interaction, mut color, mut border_color, (button_addition, button_subtraction)) in &mut interaction_query {
        //let mut text = text_query.get_mut(children[0]).unwrap();
        match *interaction {
            Interaction::Pressed => {
                //text.sections[0].value = "Press".to_string();
                *color = PRESSED_BUTTON.into();
                border_color.0 = Color::RED;
                //change the text
                for mut text in &mut text_query {
                    if button_addition.is_some(){
                        let currentcount = text.sections[1].value.parse::<i32>().unwrap();
                        //println!("{}",currentcount);
                        let currentcount = currentcount+1;
                        //let stringvalue = currentcount.to_string();
                        //println!("{}",currentcount);
                        text.sections[1].value = format!("{currentcount:.0}");                    
                    }
                    if button_subtraction.is_some(){
                        let currentcount = text.sections[1].value.parse::<i32>().unwrap();
                        //println!("{}",currentcount);
                        let currentcount = currentcount-1;
                        //let stringvalue = currentcount.to_string();
                        //println!("{}",currentcount);
                        text.sections[1].value = format!("{currentcount:.0}");                                        
                    }
                }
            }
            Interaction::Hovered => {
                //text.sections[0].value = "Hover".to_string();
                *color = HOVERED_BUTTON.into();
                border_color.0 = Color::WHITE;
            }
            Interaction::None => {
                //text.sections[0].value = "Button".to_string();
                *color = NORMAL_BUTTON.into();
                border_color.0 = Color::BLACK;
            }
        }
    }
}


fn setup(mut commands: Commands) {
    // ui camera
    commands.spawn(Camera2dBundle::default());
    // two buttons as separate nodes

    // add button
    commands
        .spawn(ButtonBundle {
            style: Style {
                //width: Val::Px(150.0),
                //height: Val::Px(65.0),
                border: UiRect::all(Val::Px(5.0)),
                // horizontally center child text
                justify_content: JustifyContent::Center,
                // vertically center child text
                position_type: PositionType::Absolute,
                top: Val::Px(5.0),
                right: Val::Px(5.0),
                //align_items: AlignItems::Center,
                ..default()
            },
            border_color: BorderColor(Color::BLACK),
            background_color: NORMAL_BUTTON.into(),
            ..default()
        })
        .with_children(|parent| {
            parent.spawn(TextBundle::from_section(
                "ADD",
                TextStyle {
                    //font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                    font_size: 40.0,
                    color: Color::rgb(0.9, 0.9, 0.9),
                    ..default()
                },
            ));
        })
        .insert(StructAdditionButton);

    // subtract button
    commands
        .spawn(ButtonBundle {
            style: Style {
                //width: Val::Px(150.0),
                //height: Val::Px(65.0),
                border: UiRect::all(Val::Px(5.0)),
                // horizontally center child text
                justify_content: JustifyContent::Center,
                // vertically center child text
                position_type: PositionType::Absolute,
                bottom: Val::Px(5.0),
                right: Val::Px(5.0),
                //align_items: AlignItems::Center,
                ..default()
            },
            border_color: BorderColor(Color::BLACK),
            background_color: NORMAL_BUTTON.into(),
            ..default()
        })
        .with_children(|parent| {
            parent.spawn(TextBundle::from_section(
                "SUBTRACT",
                TextStyle {
                    //font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                    font_size: 40.0,
                    color: Color::rgb(0.9, 0.9, 0.9),
                    ..default()
                },
            ));
        })
        .insert(StructSubtractButton);

    
    // counter text
    commands.spawn((
        // Create a TextBundle that has a Text with a list of sections.
        TextBundle::from_sections([
            TextSection::new(
                "Counter: ",
                TextStyle {
                    // This font is loaded and will be used instead of the default font.
                    //font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                    font_size: 60.0,
                    ..default()
                },
            ),
            TextSection::new(
                "0",
                TextStyle {
                    // This font is loaded and will be used instead of the default font.
                    //font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                    font_size: 60.0,
                    ..default()
                },
            ),
        ]),
        CounterText,
    ));
    
}

