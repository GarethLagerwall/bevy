use bevy::{
    prelude::*,
    sprite::{MaterialMesh2dBundle, Mesh2dHandle},
    //render::mesh::*
    //math::primitives::*
};

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_systems(Startup, setup)
        .add_systems(Update, drawgiz)
        .run();
}



/// We separated our internal representation of our Position from Bevy's Transform.
/// We apply our internal state to Bevy’s Transform in one place. This way our BallBundle can have its own representation of
/// Position and we better model our own game logic rather than the logic of Bevy’s renderer.
// fn project_positions(
//     mut positionables: Query<(&mut Transform, &Position)>
// ){
//     for (mut transform, position) in &mut positionables {
//         transform.translation = position.0;
//     }
// }

fn drawgiz(
    //mut commands: Commands,
    //mut meshes: ResMut<Assets<Mesh>>,
    //mut materials: ResMut<Assets<ColorMaterial>>,
    mut gizmos: Gizmos,
) {
    gizmos.line_2d(Vec2::new(0.,0.) , Vec2::new(50.,0.), RED);
    gizmos.ray_2d(Vec2::ZERO , Vec2::splat(50.), PINK);
    
    gizmos.arrow_2d(
        Vec2::new(0.,50.),
        Vec2::new(-100.,10.),
        GOLD,
    );

    // You can create more complex arrows using the arrow builder.
    gizmos
        .arrow_2d(Vec2::new(-20.,-10.), Vec2::from_angle(45.), GREEN)  
    //.with_double_end() //only available in Bevy 0.14
        .with_tip_length(10.);

}


fn setup(
    mut commands: Commands,
    //mut meshes: ResMut<Assets<Mesh>>,
    //mut materials: ResMut<Assets<ColorMaterial>>,
    mut gizmos: Gizmos,
) {
    commands.spawn(Camera2dBundle::default());

     // text
    commands.spawn(
        TextBundle::from_section(
            "Should see gizmos here",
            TextStyle::default(),
        )
            .with_style(Style {
                position_type: PositionType::Absolute,
                top: Val::Px(12.),
                left: Val::Px(12.),
                ..default()
            }),
    );
}

    
// #[derive(Component)]
// struct Position(Vec3);
// #[derive(Component)]
// struct FromPos(Vec3);
// #[derive(Component)]
// struct ToPos(Vec3);
// #[derive(Component, Debug)]
// struct Name(String);
// #[derive(Component)]
// struct FromSupply(String);
// #[derive(Component)]
// struct ToSink(String);
// #[derive(Component)]
// struct Width(f32);


// COLOURS //
    pub const FUCHSIA: Color = Color::rgb(1.0, 0.0, 1.0);
    /// <div style="background-color:rgb(100%, 84%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const GOLD: Color = Color::rgb(1.0, 0.84, 0.0);
    /// <div style="background-color:rgb(50%, 50%, 50%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const GRAY: Color = Color::rgb(0.5, 0.5, 0.5);
    /// <div style="background-color:rgb(0%, 100%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const GREEN: Color = Color::rgb(0.0, 1.0, 0.0);
    /// <div style="background-color:rgb(28%, 0%, 51%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const INDIGO: Color = Color::rgb(0.29, 0.0, 0.51);
    /// <div style="background-color:rgb(20%, 80%, 20%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const LIME_GREEN: Color = Color::rgb(0.2, 0.8, 0.2);
    /// <div style="background-color:rgb(50%, 0%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const MAROON: Color = Color::rgb(0.5, 0.0, 0.0);
    /// <div style="background-color:rgb(10%, 10%, 44%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const MIDNIGHT_BLUE: Color = Color::rgb(0.1, 0.1, 0.44);
    /// <div style="background-color:rgb(0%, 0%, 50%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const NAVY: Color = Color::rgb(0.0, 0.0, 0.5);
    /// <div style="background-color:rgba(0%, 0%, 0%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    #[doc(alias = "transparent")]
    pub const NONE: Color = Color::rgba(0.0, 0.0, 0.0, 0.0);
    /// <div style="background-color:rgb(50%, 50%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const OLIVE: Color = Color::rgb(0.5, 0.5, 0.0);
    /// <div style="background-color:rgb(100%, 65%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const ORANGE: Color = Color::rgb(1.0, 0.65, 0.0);
    /// <div style="background-color:rgb(100%, 27%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const ORANGE_RED: Color = Color::rgb(1.0, 0.27, 0.0);
    /// <div style="background-color:rgb(100%, 8%, 57%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const PINK: Color = Color::rgb(1.0, 0.08, 0.58);
    /// <div style="background-color:rgb(50%, 0%, 50%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const PURPLE: Color = Color::rgb(0.5, 0.0, 0.5);
    /// <div style="background-color:rgb(100%, 0%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const RED: Color = Color::rgb(1.0, 0.0, 0.0);

// #[derive(Component, Debug)]
// struct Name(String);

// #[derive(Bundle, Debug)]
// struct Entity{
//     name: String,
//     position: Vec3,
//     #[bundle]
//     circle: ShapeCircle    
// }

// #[derive(Component, Debug)]
// struct Position(Vec3);

// #[derive(Bundle)]
// struct ShapeCircle{
//     name: String,
//     size: f32,
//     color: Color,
// }


// examples
    // let color1 = Color::rgb(0.0,0.0,0.5);
    // commands.spawn(MaterialMesh2dBundle{
    //     mesh:Mesh2dHandle(meshes.add(RegularPolygon::new(width, 6))),
    //     material: materials.add(color1),
    //     transform: Transform::from_xyz(-width,0.,0.),
    //     ..default()
    // });

    // let color2 = Color::rgb(1.0,0.0,0.0);
    // commands.spawn(MaterialMesh2dBundle{
    //     mesh:Mesh2dHandle(meshes.add(RegularPolygon::new(width, 5))),
    //     material: materials.add(color2),
    //     transform: Transform::from_xyz(width,0.,1.),
    //     ..default()
    // });
