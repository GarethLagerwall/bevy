use bevy::{
    prelude::*,
    sprite::{MaterialMesh2dBundle, Mesh2dHandle},
    color::palettes::basic::{RED},
};

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_systems(Startup, setup)
        .add_systems(Update, running)
        .run();
}

//const X_EXTENT: f32 = 600.;

#[derive(Component)]
pub struct ShapeStruct;

fn running(
    mut commands: Commands,
    time: Res<Time<Real>>,
    mut exit: EventWriter<AppExit>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut query: Query<&mut  Mesh2dHandle, With<ShapeStruct>>,
){
    let time_elapsed = time.elapsed_seconds() as i32;
    if (time_elapsed/5) == 1{
        //info!("5 secs elapsed");
        for mut q in query.iter_mut(){
            //info!("{:?}",q);
            *q = Mesh2dHandle(meshes.add(Rectangle::new(100.0, 50.0)));
        }
        //return;
    }
    if (time_elapsed/10) == 1{
        exit.send(AppExit::Success);
    }

    // for q in query.iter_mut(){
    //     info!("{:?}",q);
    // }
}

fn setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    commands.spawn(Camera2dBundle::default());

    let shape = Mesh2dHandle(meshes.add(Rectangle::new(50.0, 100.0)));
    //let num_shapes = shapes.len();

    //for (i, shape) in shapes.into_iter().enumerate() {
        // Distribute colors evenly across the rainbow.
    let color = Color::Srgba(RED);

    commands.spawn((MaterialMesh2dBundle {
        mesh: shape,
        material: materials.add(color),
        ..default()
    },ShapeStruct));
    //}
}
