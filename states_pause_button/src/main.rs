//! This example uses a button to change states from paused to running.
//! it will affect the timer, but not the button-activated counter
//!
//! It displays the current FPS in the top left corner, as well as text that changes color
//! in the bottom right. For text within a scene, please see the text2d example.

use bevy::{
    // diagnostic::{DiagnosticsStore, FrameTimeDiagnosticsPlugin},
    prelude::*,
};
//use std::time::Duration;
    
//use rand::Rng;

fn main() {
    App::new()
        //.add_plugins((DefaultPlugins, FrameTimeDiagnosticsPlugin))
        .add_plugins(DefaultPlugins)
        .add_event::<PauseEvent>()
        .init_state::<MyPausedState>()
        .add_systems(Startup, setup)
        .add_systems(Update,
                     (button_system,
                      text_update_timer,
                      toggle_pause_game.run_if(on_event::<PauseEvent>()),
                      togglevisible.run_if(in_state(MyPausedState::Paused)),
                      pausetext_timer.run_if(in_state(MyPausedState::Paused)),
                      toggleinvisible.run_if(in_state(MyPausedState::Running)),
                     )
        )
        //.add_systems(FixedUpdate, text_update_timer.run_if(in_state(MyPausedState::Running)))
        .run();
}

#[derive(States, Default, Debug, Clone, PartialEq, Eq, Hash)]
enum MyPausedState {
    #[default]
    Running,
    Paused,
}

#[derive(Event)]
struct PauseEvent;

// A unit struct to help identify the seconds counter Text component
#[derive(Component)]
struct CounterText;
#[derive(Component)]
struct TimerText;
#[derive(Component)]
struct PauseText;

const NORMAL_BUTTON: Color = Color::rgb(0.15, 0.15, 0.15);
const HOVERED_BUTTON: Color = Color::rgb(0.25, 0.25, 0.25);
const PRESSED_BUTTON: Color = Color::rgb(0.35, 0.75, 0.35);

// A unit struct to help identify the add button component
#[derive(Component)]
struct StructAdditionButton;
// A unit struct to help identify the subtract button component

#[derive(Component)]
struct StructSubtractButton;

#[derive(Component)]
struct StructPauseButton;

fn button_system(
    mut interaction_query: Query<
        (
            &Interaction,
            &mut BackgroundColor,
            &mut BorderColor,
            //&Children,
            (Option<&StructAdditionButton>, Option<&StructSubtractButton>, Option<&StructPauseButton>),
        ),
        (Changed<Interaction>, With<Button>),
    >,
    mut text_query: Query<&mut Text, With<CounterText>>,
    mut evw_pause: EventWriter<PauseEvent>,
) {
    for (interaction, mut color, mut border_color, (button_addition, button_subtraction, button_pause)) in &mut interaction_query {
        match *interaction {
            Interaction::Pressed => {
                *color = PRESSED_BUTTON.into();
                border_color.0 = Color::RED;
                for mut text in &mut text_query {
                    if button_addition.is_some(){
                        let currentcount = text.sections[1].value.parse::<i32>().unwrap();
                        let currentcount = currentcount+1;
                        text.sections[1].value = format!("{currentcount:.0}");                    
                    }
                    if button_subtraction.is_some(){
                        let currentcount = text.sections[1].value.parse::<i32>().unwrap();
                        let currentcount = currentcount-1;
                        text.sections[1].value = format!("{currentcount:.0}");                                        
                    }
                }
                if button_pause.is_some(){
                    info!("pressed pause");
                    evw_pause.send(PauseEvent);
                }
            }
            Interaction::Hovered => {
                *color = HOVERED_BUTTON.into();
                border_color.0 = Color::WHITE;
            }
            Interaction::None => {
                *color = NORMAL_BUTTON.into();
                border_color.0 = Color::BLACK;
            }
        }
    }
}


fn setup(mut commands: Commands) {
    // UI camera
    commands.spawn(Camera2dBundle::default());

    commands.spawn(NodeBundle {
        style: Style {
            align_items: AlignItems::Center,
            justify_content: JustifyContent::SpaceAround,
            flex_direction: FlexDirection::Column,
            position_type: PositionType::Absolute,
            top: Val::Px(5.0),
            right: Val::Px(5.0),
            ..default()
        },
        ..default()
    }).with_children(|parent|{
        parent.spawn(
            TextBundle::from_sections([
                TextSection::new(
                    "Seconds Elapsed: ",
                    TextStyle {
                        font_size: 40.0,
                        ..default()
                    },
                ),
                TextSection::new(
                    "0",
                    TextStyle {
                        font_size: 40.0,
                        ..default()
                    },
                ),
            ])        )
            .insert(TimerText);        
        parent.spawn(ButtonBundle {
            style: Style {
                border: UiRect::all(Val::Px(5.0)),
                ..default()
            },
            border_color: BorderColor(Color::BLACK),
            background_color: NORMAL_BUTTON.into(),
            ..default()
        })
            .with_children(|build| {
                build.spawn(TextBundle::from_section(
                    "PAUSE",
                    TextStyle {
                        font_size: 40.0,
                        color: Color::rgb(0.9, 0.9, 0.9),
                        ..default()
                    },
                ));
            })
            .insert(StructPauseButton);
    });

        
    commands.spawn(NodeBundle {
            style: Style {
                align_items: AlignItems::Center,
                justify_content: JustifyContent::SpaceAround,
                flex_direction: FlexDirection::Column,
                ..default()
            },
        ..default()
    }).with_children(|parent|{
        parent.spawn((
            TextBundle::from_sections([
                TextSection::new(
                    "Counter: ",
                    TextStyle {
                        font_size: 60.0,
                        ..default()
                    },
                ),
                TextSection::new(
                    "0",
                    TextStyle {
                        font_size: 60.0,
                        ..default()
                    },
                ),
            ]),
            CounterText,
        ));
        parent.spawn(ButtonBundle {
            style: Style {
                border: UiRect::all(Val::Px(5.0)),
                ..default()
            },
            border_color: BorderColor(Color::BLACK),
            background_color: NORMAL_BUTTON.into(),
            ..default()
        })
        .with_children(|build| {
            build.spawn(TextBundle::from_section(
                "ADD",
                TextStyle {
                    font_size: 40.0,
                    color: Color::rgb(0.9, 0.9, 0.9),
                    ..default()
                },
            ));
        })
        .insert(StructAdditionButton);
        parent.spawn(ButtonBundle {
            style: Style {
                border: UiRect::all(Val::Px(5.0)),
                ..default()
            },
            border_color: BorderColor(Color::BLACK),
            background_color: NORMAL_BUTTON.into(),
            ..default()
        })
        .with_children(|build| {
            build.spawn(TextBundle::from_section(
                "SUBTRACT",
                TextStyle {
                    font_size: 40.0,
                    color: Color::rgb(0.9, 0.9, 0.9),
                    ..default()
                },
            ));
        })
        .insert(StructSubtractButton);
    });

    commands.spawn(NodeBundle {
        style: Style {
            align_items: AlignItems::Center,
            justify_content: JustifyContent::SpaceAround,
            flex_direction: FlexDirection::Column,
            position_type: PositionType::Absolute,
            bottom: Val::Px(5.0),
            right: Val::Px(5.0),
            ..default()
        },
        visibility: Visibility::Hidden,
        ..default()
    }).with_children(|parent| {
        parent.spawn((
                TextBundle::from_section(
                    "marker",
                    TextStyle {
                        font_size: 40.0,
                        color: Color::GOLD,
                        ..default()
                    },
                )
                .with_text_justify(JustifyText::Right),
                PauseText,
            ));
        });
}

fn toggle_pause_game(
    state: Res<State<MyPausedState>>,
    mut next_state: ResMut<NextState<MyPausedState>>,
    mut vtime: ResMut<Time<Virtual>>,
) {
    // https://bevy-cheatbook.github.io/programming/states.html#controlling-states
    match state.get() {
        MyPausedState::Paused => next_state.set(MyPausedState::Running),
        MyPausedState::Running => next_state.set(MyPausedState::Paused),
    }
    // https://docs.rs/bevy/0.13.2/bevy/time/struct.Time.html#method.pause
    if vtime.is_paused() {
        vtime.unpause();
    } else {
        vtime.pause();
    }
}

fn togglevisible(
    mut query: Query<&mut Visibility, With<PauseText>>,
){
    for mut visibility in &mut query{
        *visibility = Visibility::Visible;
    }
}

fn toggleinvisible(
    mut query: Query<&mut Visibility, With<PauseText>>,
){
    for mut visibility in &mut query{
        *visibility = Visibility::Hidden;
    }
}

fn text_update_timer(
    time: Res<Time<Virtual>>,
    //vtime: Res<Virtual>,
    mut query: Query<&mut Text, With<TimerText>>,
) {
    let time_elapsed = time.elapsed_seconds();
//    let mut stopwatch = Stopwatch::new();
//    let seconds_counted = stopwatch.elapsed_secs();
    //let gametime = time.delta();
    for mut text in &mut query {
        // Update the value of the second section
        text.sections[1].value = format!("{:.0?}",time_elapsed);
    }
}

fn pausetext_timer(
    vtime: Res<Time<Virtual>>,
    //vtime: Res<Virtual>,
    time: Res<Time<Real>>,
    mut query: Query<&mut Text, With<PauseText>>,
) {
    //info!("running paused timer");
    let time_elapsed = time.elapsed_seconds() as i32;
    let vtime_elapsed = vtime.elapsed_seconds() as i32;
    let ptime= time_elapsed - vtime_elapsed;
//    let mut stopwatch = Stopwatch::new();
//    let seconds_counted = stopwatch.elapsed_secs();
    //let gametime = time.delta();
    for mut text in &mut query {
        //info!("{}",text.sections[0].value);
        // Update the value of the second section
        text.sections[0].value = format!("Paused Time: {:.0?}",ptime);
    }
//    let text = query.single_mut();
//    info!("{}",text.sections[0].value);
}


