use bevy::{
    prelude::*,
    sprite::{MaterialMesh2dBundle, Mesh2dHandle},
    color::palettes::basic::{RED},
};

use bevy_tweening::{lens::*,*};
//use bevy_tweening::lens::TransformPositionLens;
use std::time::Duration;


fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugins(TweeningPlugin)
        .add_systems(Startup, setup)
        .add_systems(Update, running)
        .run();
}

//const X_EXTENT: f32 = 600.;

#[derive(Component)]
pub struct ShapeStruct;

fn running(
    mut commands: Commands,
    time: Res<Time<Real>>,
    mut exit: EventWriter<AppExit>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut query: Query<&mut  Mesh2dHandle, With<ShapeStruct>>,
    mut qanim: Query<&mut Animator<Transform>, With<ShapeStruct>>,
){
    let time_elapsed = time.elapsed_seconds() as i32;
    if (time_elapsed/5) == 1{
        //info!("5 secs elapsed");
        for mut q in query.iter_mut(){
            *q = Mesh2dHandle(meshes.add(Rectangle::new(20.0, 10.0)));
        }
        for mut anim in qanim.iter_mut(){
            let tween = Tween::new(
                EaseFunction::QuadraticInOut,
                Duration::from_secs(1),
                TransformPositionLens {
                    start: Vec3::new(50.,50.,0.),
                    end: Vec3::new(-100., -200., 0.),
                },
            )
                .with_repeat_count(RepeatCount::For(Duration::new(5,0)))
                .with_repeat_strategy(RepeatStrategy::Repeat);
            anim.set_tweenable(tween);
        }
        //return;
    }
    // if (time_elapsed/10) == 1{
    //     exit.send(AppExit::Success);
    // }

    // for q in query.iter_mut(){
    //     info!("{:?}",q);
    // }
}

fn setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    commands.spawn(Camera2dBundle::default());


    // Create a single animation (tween) to move an entity.
    let tween = Tween::new(
        // Use a quadratic easing on both endpoints.
        EaseFunction::QuadraticInOut,
        // Animation time.
        Duration::from_secs(1),
        // The lens gives access to the Transform component of the Entity,
        // for the Animator to animate it. It also contains the start and
        // end values respectively associated with the progress ratios 0. and 1.
        TransformPositionLens {
            start: Vec3::ZERO,
            end: Vec3::new(100., 200., 0.),
        },
    )
        .with_repeat_count(RepeatCount::For(Duration::new(5,0)))
        .with_repeat_strategy(RepeatStrategy::Repeat);

    let shape = Mesh2dHandle(meshes.add(Rectangle::new(10.0, 20.0)));
    //let num_shapes = shapes.len();

    //for (i, shape) in shapes.into_iter().enumerate() {
        // Distribute colors evenly across the rainbow.
    let color = Color::Srgba(RED);

    commands.spawn((MaterialMesh2dBundle {
        mesh: shape,
        material: materials.add(color),
        ..default()
    },ShapeStruct))
        .insert(Animator::new(tween));
    //}
}
