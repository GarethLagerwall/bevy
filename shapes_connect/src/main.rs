use bevy::{
    prelude::*,
    sprite::{MaterialMesh2dBundle, Mesh2dHandle},
    //render::mesh::*
    //math::primitives::*
};

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_systems(Startup, (setup,
                               createrect.after(setup),
                               query_system.after(createrect)))
        .add_systems(Update, project_positions)
        .run();
}


fn createrect(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    query_names: Query<(&Name, &Position)>,
    mut query_rect: Query<(Entity, &mut FromPos, &mut ToPos, &FromPointA, &ToPointB, &Width)>,
    //query_rect: Query<(&FromPointA, &ToPointB)>,
){
    info!("CREATING RECTANGLE");
    for (entity, mut frompos, mut topos, fromsup, topointB, basewidth) in &mut query_rect{
    //for (fromsup, topointB) in &query_rect{
        info!("{},{}",fromsup.0,topointB.0);
        let mut startpos = Vec3::new(0.,0.,0.) ;
        let mut endpos = Vec3::new(0.,0.,0.);
        for (name, position) in &query_names{
            if name.0 == fromsup.0{
                info!("Matched From: {} and Name: {}",fromsup.0,name.0);
                startpos = position.0;
            }
            else if name.0 == topointB.0{
                info!("Matched To: {} and Name: {}",topointB.0,name.0);
                endpos = position.0;
            }
            //info!("\n\nENTITY NAME: {} Position {} \n\n",endpos, startpos);
        }
        //Changing the from and to positions to reflect the associated circles
        *frompos = FromPos(startpos);
        *topos = ToPos(endpos);
        info!("new startpos: {}, endpos: {}", frompos.0, topos.0);

        //
        // create transforms etc. for rectangle
        //
        //info!("{}{}",startpos[0],endpos[0]);
        let length = ((startpos[0]-endpos[0]).powi(2)+(startpos[1]-endpos[1]).powi(2)).sqrt();
        let centrepoint  = ((startpos[0] + endpos[0]) / 2.0, (startpos[1] + endpos[1]) / 2.0);
        let degree = ((startpos[1] - endpos[1]) / (startpos[0] - endpos[0])).atan();
        info!("length: {}, centre: {:?}, rotation: {}", length, centrepoint, degree);
        // transforms
        let mut recttransform = Transform::from_xyz(centrepoint.0, centrepoint.1, -1.0);
        recttransform.rotation = Quat::from_rotation_z(degree);

        // generate rectangle
        let rectgraphic = commands.spawn(MaterialMesh2dBundle{
            mesh: Mesh2dHandle(meshes.add(Rectangle::new(length, basewidth.0))), 
            material: materials.add(NAVY),
            transform: recttransform,
            ..default()
        }).id();
        
        commands.entity(entity).push_children(&[rectgraphic]);
    }
}

fn query_system(
    query_names: Query<&Name>,
){
    for name in &query_names{
        info!("\n\nENTITY NAME: {} \n\n",name.0);
    }
}


/// We separated our internal representation of our Position from Bevy's Transform.
/// We apply our internal state to Bevy’s Transform in one place. This way our BallBundle can have its own representation of
/// Position and we better model our own game logic rather than the logic of Bevy’s renderer.
fn project_positions(
    mut positionables: Query<(&mut Transform, &Position)>
){
    for (mut transform, position) in &mut positionables {
        transform.translation = position.0;
    }
}

fn setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    commands.spawn(Camera2dBundle::default());

    let gensize: f32 = 50.0;
    
    add_circles(&mut commands, &mut meshes, &mut materials, Name("redcircle".to_string()), RED,gensize, Position(Vec3::new(-gensize*2.,200.,0.)));

    add_circles(&mut commands, &mut meshes, &mut materials, Name("goldcircle".to_string()), GOLD,gensize, Position(Vec3::new(gensize*2.,0., 0.)));

    // create base rectangle
    // use SpatialBundle to enable transform, visibility, etc.
    // see: https://bevyengine.org/learn/errors/b0004/
    commands.spawn(SpatialBundle::default())
        .insert(RectLink{
            name : Name("testingrect".to_string()),
            from_pointA: FromPointA("redcircle".to_string()),
            frompos: FromPos(Vec3::new(0.,0.,0.)),
            to_pointB: ToPointB("goldcircle".to_string()),
            topos: ToPos(Vec3::new(0.,0.,0.)),
            basewidth: Width(25.),
        });
    
}


fn add_circles(
    commands: &mut Commands,
    meshes: &mut ResMut<Assets<Mesh>>,
    materials: &mut ResMut<Assets<ColorMaterial>>,
    shapename: Name,
    shapecolor: Color,
    shapesize: f32,
    position: Position,
){
    commands
        .spawn(MaterialMesh2dBundle{
            mesh:Mesh2dHandle(meshes.add(Circle::new(shapesize))),
            material: materials.add(shapecolor),
            ..default()
        })
        .insert(position)
        .insert(shapename);
}

    
#[derive(Component)]
struct Position(Vec3);
#[derive(Component)]
struct FromPos(Vec3);
#[derive(Component)]
struct ToPos(Vec3);
#[derive(Component, Debug)]
struct Name(String);
#[derive(Component)]
struct FromPointA(String);
#[derive(Component)]
struct ToPointB(String);
#[derive(Component)]
struct Width(f32);

#[derive(Bundle)]
struct RectLink{
    name : Name,
    from_pointA: FromPointA,
    frompos: FromPos,
    to_pointB: ToPointB,
    topos: ToPos,
    basewidth: Width,
}

// implement default (zero) initialization
// see: https://bevy-cheatbook.github.io/programming/bundle.html
impl Default for RectLink{
    fn default() -> Self {
        Self {
            name: Name("temp".to_string()),
            from_pointA: FromPointA("temp".to_string()),
            frompos: FromPos(Vec3::new(0.,0.,0.)),
            to_pointB: ToPointB("temp".to_string()),
            topos: ToPos(Vec3::new(0.,0.,0.)),
            basewidth: Width(0.),
        }
    }
}


// COLOURS //
    pub const FUCHSIA: Color = Color::rgb(1.0, 0.0, 1.0);
    /// <div style="background-color:rgb(100%, 84%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const GOLD: Color = Color::rgb(1.0, 0.84, 0.0);
    /// <div style="background-color:rgb(50%, 50%, 50%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const GRAY: Color = Color::rgb(0.5, 0.5, 0.5);
    /// <div style="background-color:rgb(0%, 100%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const GREEN: Color = Color::rgb(0.0, 1.0, 0.0);
    /// <div style="background-color:rgb(28%, 0%, 51%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const INDIGO: Color = Color::rgb(0.29, 0.0, 0.51);
    /// <div style="background-color:rgb(20%, 80%, 20%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const LIME_GREEN: Color = Color::rgb(0.2, 0.8, 0.2);
    /// <div style="background-color:rgb(50%, 0%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const MAROON: Color = Color::rgb(0.5, 0.0, 0.0);
    /// <div style="background-color:rgb(10%, 10%, 44%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const MIDNIGHT_BLUE: Color = Color::rgb(0.1, 0.1, 0.44);
    /// <div style="background-color:rgb(0%, 0%, 50%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const NAVY: Color = Color::rgb(0.0, 0.0, 0.5);
    /// <div style="background-color:rgba(0%, 0%, 0%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    #[doc(alias = "transparent")]
    pub const NONE: Color = Color::rgba(0.0, 0.0, 0.0, 0.0);
    /// <div style="background-color:rgb(50%, 50%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const OLIVE: Color = Color::rgb(0.5, 0.5, 0.0);
    /// <div style="background-color:rgb(100%, 65%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const ORANGE: Color = Color::rgb(1.0, 0.65, 0.0);
    /// <div style="background-color:rgb(100%, 27%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const ORANGE_RED: Color = Color::rgb(1.0, 0.27, 0.0);
    /// <div style="background-color:rgb(100%, 8%, 57%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const PINK: Color = Color::rgb(1.0, 0.08, 0.58);
    /// <div style="background-color:rgb(50%, 0%, 50%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const PURPLE: Color = Color::rgb(0.5, 0.0, 0.5);
    /// <div style="background-color:rgb(100%, 0%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const RED: Color = Color::rgb(1.0, 0.0, 0.0);

// #[derive(Component, Debug)]
// struct Name(String);

// #[derive(Bundle, Debug)]
// struct Entity{
//     name: String,
//     position: Vec3,
//     #[bundle]
//     circle: ShapeCircle    
// }

// #[derive(Component, Debug)]
// struct Position(Vec3);

// #[derive(Bundle)]
// struct ShapeCircle{
//     name: String,
//     size: f32,
//     color: Color,
// }


// examples
    // let color1 = Color::rgb(0.0,0.0,0.5);
    // commands.spawn(MaterialMesh2dBundle{
    //     mesh:Mesh2dHandle(meshes.add(RegularPolygon::new(width, 6))),
    //     material: materials.add(color1),
    //     transform: Transform::from_xyz(-width,0.,0.),
    //     ..default()
    // });

    // let color2 = Color::rgb(1.0,0.0,0.0);
    // commands.spawn(MaterialMesh2dBundle{
    //     mesh:Mesh2dHandle(meshes.add(RegularPolygon::new(width, 5))),
    //     material: materials.add(color2),
    //     transform: Transform::from_xyz(width,0.,1.),
    //     ..default()
    // });
