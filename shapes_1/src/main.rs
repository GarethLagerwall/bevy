use bevy::{
    prelude::*,
    sprite::{MaterialMesh2dBundle, Mesh2dHandle},
};

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_systems(Startup, setup)
        .run();
}

fn setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    commands.spawn(Camera2dBundle::default());


    let width = 50.0;
    
    let color1 = Color::rgb(0.0,0.0,0.5);
    commands.spawn(MaterialMesh2dBundle{
        mesh:Mesh2dHandle(meshes.add(RegularPolygon::new(width, 6))),
        material: materials.add(color1),
        transform: Transform::from_xyz(-width,0.,0.),
        ..default()
    });

    let color2 = Color::rgb(1.0,0.0,0.0);
    commands.spawn(MaterialMesh2dBundle{
        mesh:Mesh2dHandle(meshes.add(RegularPolygon::new(width, 5))),
        material: materials.add(color2),
        transform: Transform::from_xyz(width,0.,1.),
        ..default()
    });
}



    // pub const FUCHSIA: Color = Color::rgb(1.0, 0.0, 1.0);
    // /// <div style="background-color:rgb(100%, 84%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    // pub const GOLD: Color = Color::rgb(1.0, 0.84, 0.0);
    // /// <div style="background-color:rgb(50%, 50%, 50%); width: 10px; padding: 10px; border: 1px solid;"></div>
    // pub const GRAY: Color = Color::rgb(0.5, 0.5, 0.5);
    // /// <div style="background-color:rgb(0%, 100%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    // pub const GREEN: Color = Color::rgb(0.0, 1.0, 0.0);
    // /// <div style="background-color:rgb(28%, 0%, 51%); width: 10px; padding: 10px; border: 1px solid;"></div>
    // pub const INDIGO: Color = Color::rgb(0.29, 0.0, 0.51);
    // /// <div style="background-color:rgb(20%, 80%, 20%); width: 10px; padding: 10px; border: 1px solid;"></div>
    // pub const LIME_GREEN: Color = Color::rgb(0.2, 0.8, 0.2);
    // /// <div style="background-color:rgb(50%, 0%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    // pub const MAROON: Color = Color::rgb(0.5, 0.0, 0.0);
    // /// <div style="background-color:rgb(10%, 10%, 44%); width: 10px; padding: 10px; border: 1px solid;"></div>
    // pub const MIDNIGHT_BLUE: Color = Color::rgb(0.1, 0.1, 0.44);
    // /// <div style="background-color:rgb(0%, 0%, 50%); width: 10px; padding: 10px; border: 1px solid;"></div>
    // pub const NAVY: Color = Color::rgb(0.0, 0.0, 0.5);
    // /// <div style="background-color:rgba(0%, 0%, 0%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    // #[doc(alias = "transparent")]
    // pub const NONE: Color = Color::rgba(0.0, 0.0, 0.0, 0.0);
    // /// <div style="background-color:rgb(50%, 50%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    // pub const OLIVE: Color = Color::rgb(0.5, 0.5, 0.0);
    // /// <div style="background-color:rgb(100%, 65%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    // pub const ORANGE: Color = Color::rgb(1.0, 0.65, 0.0);
    // /// <div style="background-color:rgb(100%, 27%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    // pub const ORANGE_RED: Color = Color::rgb(1.0, 0.27, 0.0);
    // /// <div style="background-color:rgb(100%, 8%, 57%); width: 10px; padding: 10px; border: 1px solid;"></div>
    // pub const PINK: Color = Color::rgb(1.0, 0.08, 0.58);
    // /// <div style="background-color:rgb(50%, 0%, 50%); width: 10px; padding: 10px; border: 1px solid;"></div>
    // pub const PURPLE: Color = Color::rgb(0.5, 0.0, 0.5);
    // /// <div style="background-color:rgb(100%, 0%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    // pub const RED: Color = Color::rgb(1.0, 0.0, 0.0);
    
    // let shapes = [
    //     Mesh2dHandle(meshes.add(Circle { radius: 50.0 })),
    //     Mesh2dHandle(meshes.add(Ellipse::new(25.0, 50.0))),
    //     Mesh2dHandle(meshes.add(Capsule2d::new(25.0, 50.0))),
    //     Mesh2dHandle(meshes.add(Rectangle::new(50.0, 100.0))),
    //     Mesh2dHandle(meshes.add(RegularPolygon::new(50.0, 6))),
    //     Mesh2dHandle(meshes.add(Triangle2d::new(
    //         Vec2::Y * 50.0,
    //         Vec2::new(-50.0, -50.0),
    //         Vec2::new(50.0, -50.0),
    //     ))),
    // ];
    // let num_shapes = shapes.len();

    // for (i, shape) in shapes.into_iter().enumerate() {
    //     // Distribute colors evenly across the rainbow.
    //     let color = Color::hsl(360. * i as f32 / num_shapes as f32, 0.95, 0.7);

    //     commands.spawn(MaterialMesh2dBundle {
    //         mesh: shape,
    //         material: materials.add(color),
    //         transform: Transform::from_xyz(
    //             // Distribute shapes from -X_EXTENT to +X_EXTENT.
    //             -X_EXTENT / 2. + i as f32 / (num_shapes - 1) as f32 * X_EXTENT,
    //             0.0,
    //             0.0,
    //         ),
    //         ..default()
    //     });
    // }
