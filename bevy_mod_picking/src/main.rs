use bevy::{
    prelude::*,
    sprite::{MaterialMesh2dBundle, Mesh2dHandle},
    winit::WinitSettings,
    //render::mesh::*
    //math::primitives::*
    text::{BreakLineOn, Text2dBounds},
};
use bevy_mod_picking::prelude::*;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugins(DefaultPickingPlugins) // Includes a mesh raycasting backend by default
        .insert_resource(WinitSettings::desktop_app())
        .add_event::<SpawnButtons>()
        .add_systems(Startup,
                     (setup,
                      spawnbutts.after(setup),
                      query_system.after(spawnbutts)))
        .add_systems(Update,
                     (button_system,
                      project_positions))
        .run();
}


const NORMAL_BUTTON: Color = Color::rgb(0.15, 0.15, 0.15);
const HOVERED_BUTTON: Color = Color::rgb(0.25, 0.25, 0.25);
const PRESSED_BUTTON: Color = Color::rgb(0.35, 0.75, 0.35);

#[derive(Component)]
struct Position(Vec3);
#[derive(Component, Debug)]
struct Name(String);
// A unit struct to help identify the changing counter component
#[derive(Component)]
struct CounterText;
// A unit struct to help identify the add button component
#[derive(Component)]
struct StructAdditionButton;
// A unit struct to help identify the subtract button component
#[derive(Component)]
struct StructSubtractButton;
#[derive(Event)]
struct SpawnButtons(Entity);


#[derive(Bundle)]
struct ClusterBundle{
    name: Name,
    position: Position,
    #[bundle()]
    spatial: SpatialBundle,
    //#[bundle()]
    //node: NodeBundle,
}
//commands.spawn(SpatialBundle::default())
// implement default (zero) initialization
// see: https://bevy-cheatbook.github.io/programming/bundle.html
impl Default for ClusterBundle{
    fn default() -> Self {
        Self {
            name: Name("temp".to_string()),
            position: Position(Vec3::new(0.,0.,0.)),
            spatial: SpatialBundle::default(),
            //node: NodeBundle::default(),
        }
    }
}

fn setup(
    mut commands: Commands,
    //mut meshes: ResMut<Assets<Mesh>>,
    //mut materials: ResMut<Assets<ColorMaterial>>,
    mut but_spawn: EventWriter<SpawnButtons>,
) {
    commands.spawn(Camera2dBundle::default());

    let cluster = commands.spawn(
        ClusterBundle{
            name: Name("spawn1".to_string()),
            position: Position(Vec3::new(-300., 300., 0.)),
            ..default()
        }).id();

    info!("sending event");
    but_spawn.send(SpawnButtons(cluster));

    let cluster2 = commands.spawn(
        ClusterBundle{
            name: Name("spawn2".to_string()),
            position: Position(Vec3::new(300., -300., 0.)),
            ..default()
        }).id();
    but_spawn.send(SpawnButtons(cluster2));

    // // create base rectangle
    // // use SpatialBundle to enable transform, visibility, etc.
    // // see: https://bevyengine.org/learn/errors/b0004/
    // commands.spawn(SpatialBundle::default())
    //     .insert(RectLink{
    //         name : Name("testingrect".to_string()),
    //         from_supply: FromSupply("redcircle".to_string()),
    //         frompos: FromPos(Vec3::new(0.,0.,0.)),
    //         to_sink: ToSink("goldcircle".to_string()),
    //         topos: ToPos(Vec3::new(0.,0.,0.)),
    //         basewidth: Width(25.),
    //     });
    
}


/// spawn buttons associated with parent entity 
fn spawnbutts(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut but_spawn: EventReader<SpawnButtons>,
){
    info!("running spawnbutts");
    for ent in but_spawn.read(){
        info!("{:?}",ent.0);

        //
        // need to use text2dbundle
        // https://github.com/bevyengine/bevy/discussions/6521#discussioncomment-4091774
        // textbundle is only for UI
        // 
        let box_size = Vec2::new(50.0, 50.0);
        //let box_position = Vec3::new(-50.0, 0.0, 0.);
        let addition = commands.spawn(MaterialMesh2dBundle {
            mesh: Mesh2dHandle(meshes.add(Circle { radius: 50.0 })),
            material: materials.add(RED),
            // transform: Transform::from_xyz(
            //     // Distribute shapes from -X_EXTENT/2 to +X_EXTENT/2.
            //     -X_EXTENT / 2. + i as f32 / (num_shapes - 1) as f32 * X_EXTENT,
            //     0.0,
            //     0.0,
            // ),
            ..default()
        })
            .with_children(|builder| {
                builder.spawn(Text2dBundle {
                    text: Text {
                        sections: vec![TextSection::new(
                            "ADDING",
                            TextStyle {
                                //font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                                font_size: 20.0,
                                color: Color::rgb(0.9, 0.9, 0.9),
                                ..default()
                            },
//                            slightly_smaller_text_style.clone(),
                        )],
                        justify: JustifyText::Left,
                        linebreak_behavior: BreakLineOn::WordBoundary,
                    },
                    text_2d_bounds: Text2dBounds {
                        // Wrap text in the rectangle
                        size: box_size,
                    },
                    // ensure the text is drawn on top of the box
                    transform: Transform::from_translation(Vec3::Z),
                    ..default()
                });
            })
            .insert(Position(Vec3::new(-50.0, 0.0, 0.)))
            .insert(StructAdditionButton)
            .insert(PickableBundle::default())
            .insert(Name("adding".to_string()))
            .id();

        
        let subtract = commands.spawn(MaterialMesh2dBundle {
            mesh: Mesh2dHandle(meshes.add(Circle { radius: 50.0 })),
            material: materials.add(NAVY),
            // transform: Transform::from_xyz(
            //     // Distribute shapes from -X_EXTENT/2 to +X_EXTENT/2.
            //     -X_EXTENT / 2. + i as f32 / (num_shapes - 1) as f32 * X_EXTENT,
            //     0.0,
            //     0.0,
            // ),
            ..default()
        })
            .with_children(|parent| {
                parent.spawn(Text2dBundle {
                    text: Text {
                        sections: vec![TextSection::new(
                            "SUBTRACT",
                            TextStyle{
                                font_size: 20.0,
                                color: Color::rgb(0.9, 0.9, 0.9),
                                ..default()                                
                            },
                            //slightly_smaller_text_style.clone(),
                        )],
                        justify: JustifyText::Left,
                        linebreak_behavior: BreakLineOn::WordBoundary,
                    },
                    text_2d_bounds: Text2dBounds {
                        // Wrap text in the rectangle
                        size: box_size,
                    },
                    // ensure the text is drawn on top of the box
                    transform: Transform::from_translation(Vec3::Z),
                    ..default()
                });
            })
            .insert(StructSubtractButton)
            .insert(PickableBundle::default())
            .insert(Position(Vec3::new(50.,0.,0.)))
            .insert(Name("subtracting".to_string()))
            //.insert(SpatialBundle::default())
            .id();

        info!("inserting children to {:?}",ent.0);
        commands.entity(ent.0).push_children(&[addition, subtract]);
    }
}



fn button_system(
    mut interaction_query: Query<
        (
            &Interaction,
            &mut BackgroundColor,
            &mut BorderColor,
            //&Children,
            (Option<&StructAdditionButton>,Option<&StructSubtractButton>),
        ),
        (Changed<Interaction>, With<Button>),
    >,
    mut text_query: Query<&mut Text, With<CounterText>>,
) {
    info!("button select");
    for (interaction, mut color, mut border_color, (button_addition, button_subtraction)) in &mut interaction_query {
        //let mut text = text_query.get_mut(children[0]).unwrap();
        match *interaction {
            Interaction::Pressed => {
                //text.sections[0].value = "Press".to_string();
                *color = PRESSED_BUTTON.into();
                border_color.0 = Color::RED;
                //change the text
                // for mut text in &mut text_query {
                //     if button_addition.is_some(){
                //         let currentcount = text.sections[1].value.parse::<i32>().unwrap();
                //         //println!("{}",currentcount);
                //         let currentcount = currentcount+1;
                //         //let stringvalue = currentcount.to_string();
                //         //println!("{}",currentcount);
                //         text.sections[1].value = format!("{currentcount:.0}");                    
                //     }
                //     if button_subtraction.is_some(){
                //         let currentcount = text.sections[1].value.parse::<i32>().unwrap();
                //         //println!("{}",currentcount);
                //         let currentcount = currentcount-1;
                //         //let stringvalue = currentcount.to_string();
                //         //println!("{}",currentcount);
                //         text.sections[1].value = format!("{currentcount:.0}");                                        
                //     }
                // }
            }
            Interaction::Hovered => {
                //text.sections[0].value = "Hover".to_string();
                *color = HOVERED_BUTTON.into();
                border_color.0 = Color::WHITE;
            }
            Interaction::None => {
                //text.sections[0].value = "Button".to_string();
                *color = NORMAL_BUTTON.into();
                border_color.0 = Color::BLACK;
            }
        }
    }
}


// fn createrect(
//     mut commands: Commands,
//     mut meshes: ResMut<Assets<Mesh>>,
//     mut materials: ResMut<Assets<ColorMaterial>>,
//     query_names: Query<(&Name, &Position)>,
//     mut query_rect: Query<(Entity, &mut FromPos, &mut ToPos, &FromSupply, &ToSink, &Width)>,
//     //query_rect: Query<(&FromSupply, &ToSink)>,
// ){
//     info!("CREATING RECTANGLE");
//     for (entity, mut frompos, mut topos, fromsup, tosink, basewidth) in &mut query_rect{
//     //for (fromsup, tosink) in &query_rect{
//         info!("{},{}",fromsup.0,tosink.0);
//         let mut startpos = Vec3::new(0.,0.,0.) ;
//         let mut endpos = Vec3::new(0.,0.,0.);
//         for (name, position) in &query_names{
//             if name.0 == fromsup.0{
//                 info!("Matched From: {} and Name: {}",fromsup.0,name.0);
//                 startpos = position.0;
//             }
//             else if name.0 == tosink.0{
//                 info!("Matched To: {} and Name: {}",tosink.0,name.0);
//                 endpos = position.0;
//             }
//             //info!("\n\nENTITY NAME: {} Position {} \n\n",endpos, startpos);
//         }
//         //Changing the from and to positions to reflect the associated circles
//         *frompos = FromPos(startpos);
//         *topos = ToPos(endpos);
//         info!("new startpos: {}, endpos: {}", frompos.0, topos.0);        //
//         // create transforms etc. for rectangle
//         //info!("{}{}",startpos[0],endpos[0]);
//         let length = ((startpos[0]-endpos[0]).powi(2)+(startpos[1]-endpos[1]).powi(2)).sqrt();
//         let centrepoint  = ((startpos[0] + endpos[0]) / 2.0, (startpos[1] + endpos[1]) / 2.0);
//         let degree = ((startpos[1] - endpos[1]) / (startpos[0] - endpos[0])).atan();
//         info!("length: {}, centre: {:?}, rotation: {}", length, centrepoint, degree);
//         // transforms
//         let mut recttransform = Transform::from_xyz(centrepoint.0, centrepoint.1, -1.0);
//         recttransform.rotation = Quat::from_rotation_z(degree);
//         // generate rectangle
//         let rectgraphic = commands.spawn(MaterialMesh2dBundle{
//             mesh: Mesh2dHandle(meshes.add(Rectangle::new(length, basewidth.0))), 
//             material: materials.add(NAVY),
//             transform: recttransform,
//             ..default()
//         }).id();        
//         commands.entity(entity).push_children(&[rectgraphic]);
//     }
// }

fn query_system(
    query_names: Query<&Name>,
){
    for name in &query_names{
        info!("\n\nENTITY NAME: {} \n\n",name.0);
    }
}


/// We separated our internal representation of our Position from Bevy's Transform.
/// We apply our internal state to Bevy’s Transform in one place. This way our BallBundle can have its own representation of
/// Position and we better model our own game logic rather than the logic of Bevy’s renderer.
fn project_positions(
    mut positionables: Query<(&mut Transform, &Position)>
){
    for (mut transform, position) in &mut positionables {
        transform.translation = position.0;
    }
}



// fn add_circles(
//     commands: &mut Commands,
//     meshes: &mut ResMut<Assets<Mesh>>,
//     materials: &mut ResMut<Assets<ColorMaterial>>,
//     shapename: Name,
//     shapecolor: Color,
//     shapesize: f32,
//     position: Position,
// ){
//     commands
//         .spawn(MaterialMesh2dBundle{
//             mesh:Mesh2dHandle(meshes.add(Circle::new(shapesize))),
//             material: materials.add(shapecolor),
//             ..default()
//         })
//         .insert(position)
//         .insert(shapename);
// }

    






// #[derive(Bundle)]
// struct RectLink{
//     name : Name,
//     from_supply: FromSupply,
//     frompos: FromPos,
//     to_sink: ToSink,
//     topos: ToPos,
//     basewidth: Width,
// }

// // implement default (zero) initialization
// // see: https://bevy-cheatbook.github.io/programming/bundle.html
// impl Default for RectLink{
//     fn default() -> Self {
//         Self {
//             name: Name("temp".to_string()),
//             from_supply: FromSupply("temp".to_string()),
//             frompos: FromPos(Vec3::new(0.,0.,0.)),
//             to_sink: ToSink("temp".to_string()),
//             topos: ToPos(Vec3::new(0.,0.,0.)),
//             basewidth: Width(0.),
//         }
//     }
// }


// COLOURS //
    pub const FUCHSIA: Color = Color::rgb(1.0, 0.0, 1.0);
    /// <div style="background-color:rgb(100%, 84%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const GOLD: Color = Color::rgb(1.0, 0.84, 0.0);
    /// <div style="background-color:rgb(50%, 50%, 50%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const GRAY: Color = Color::rgb(0.5, 0.5, 0.5);
    /// <div style="background-color:rgb(0%, 100%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const GREEN: Color = Color::rgb(0.0, 1.0, 0.0);
    /// <div style="background-color:rgb(28%, 0%, 51%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const INDIGO: Color = Color::rgb(0.29, 0.0, 0.51);
    /// <div style="background-color:rgb(20%, 80%, 20%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const LIME_GREEN: Color = Color::rgb(0.2, 0.8, 0.2);
    /// <div style="background-color:rgb(50%, 0%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const MAROON: Color = Color::rgb(0.5, 0.0, 0.0);
    /// <div style="background-color:rgb(10%, 10%, 44%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const MIDNIGHT_BLUE: Color = Color::rgb(0.1, 0.1, 0.44);
    /// <div style="background-color:rgb(0%, 0%, 50%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const NAVY: Color = Color::rgb(0.0, 0.0, 0.5);
    /// <div style="background-color:rgba(0%, 0%, 0%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    #[doc(alias = "transparent")]
    pub const NONE: Color = Color::rgba(0.0, 0.0, 0.0, 0.0);
    /// <div style="background-color:rgb(50%, 50%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const OLIVE: Color = Color::rgb(0.5, 0.5, 0.0);
    /// <div style="background-color:rgb(100%, 65%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const ORANGE: Color = Color::rgb(1.0, 0.65, 0.0);
    /// <div style="background-color:rgb(100%, 27%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const ORANGE_RED: Color = Color::rgb(1.0, 0.27, 0.0);
    /// <div style="background-color:rgb(100%, 8%, 57%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const PINK: Color = Color::rgb(1.0, 0.08, 0.58);
    /// <div style="background-color:rgb(50%, 0%, 50%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const PURPLE: Color = Color::rgb(0.5, 0.0, 0.5);
    /// <div style="background-color:rgb(100%, 0%, 0%); width: 10px; padding: 10px; border: 1px solid;"></div>
    pub const RED: Color = Color::rgb(1.0, 0.0, 0.0);
